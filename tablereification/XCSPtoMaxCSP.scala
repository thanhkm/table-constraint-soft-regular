package oscar.thesis.tablereification

import java.io.File

import oscar.cp._
import oscar.cp.constraints.tables._
import oscar.cp.core.Constraint
import oscar.cp.core.variables.CPIntVar
import oscar.cp.xcsp.modeling.DefaultConstraints

import scala.sys.process._

object XCSPtoMaxCSP extends App {

  val xcspToMaxCSPSolver = new XCSPToMaxCSPSolver with DefaultConstraints {
    override def table(x: Array[CPIntVar], tuples: Array[Array[Int]]): Constraint = new TableSTR2(x, tuples)
    override def tableNe(x: Array[CPIntVar], tuples: Array[Array[Int]]): Constraint = new TableSTRNe(x, tuples)
    override def tableReif(x: Array[CPIntVar], tuples: Array[Array[Int]], b: CPBoolVar): Constraint = new TableSTR2Reif(x, tuples, b)
    //override def tableNeReif(x: Array[CPIntVar], tuples: Array[Array[Int]], b: CPBoolVar): Constraint = new TableSTR2Reif(x, tuples, b, isPositive=false)
  }

  var fileName = args(0)
  val timeout = if (args.length > 1) args(1).toInt else 1000

  var toRemove = false
  if (fileName.contains(".bz2")) {
    toRemove = true
    "bzip2 -k -d " + fileName !

    fileName = fileName.substring(0, fileName.length-4)
  }
  println(fileName)

  val (cp, vars, reifVars) = xcspToMaxCSPSolver.model(new File(fileName))
  if (toRemove) {
    //println("removed:" + fileName)
    "rm -f " + fileName !
  }


  println("nbVariables: " + vars.length + " nbConstraints: " + reifVars.length)

  val ob = -sum(reifVars) + reifVars.length
  cp.minimize(ob)


  cp.onSolution {
    //println(ob.value)
    //println(reifVars.toSeq.sortBy(v => (v.name.length, v.name)).map(v => v.name+ ":" + v.value).mkString(" "))
    println("o " + ob.value)
    println("v " + vars.toSeq.sortBy(v => (v.name.length, v.name)).map(v => v.value).mkString(" "))
  }

  val variables = reifVars ++ vars
  cp.search {
    binaryLastConflict(variables, variables(_).size, variables(_).max)
    //conflictOrderingSearch(variables, variables(_).size, variables(_).max)
  }

  cp.silent = true
  val stats = cp.start(timeLimit = timeout)

  println(stats)
  if (stats.completed) {
    println("s OPTIMUM FOUND")
  }

  println("d NODES " + stats.nFails)
  println("d TIMES " + stats.time)

}
