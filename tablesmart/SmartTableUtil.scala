package oscar.thesis.tablesmart

import oscar.cp.constraints.tables._
import oscar.cp.core.Constraint
import oscar.cp.core.variables.CPIntVar

import scala.collection.mutable.ArrayBuffer
import scala.io.Source


/**
  * @author ThanhKM thanhkhongminh@gmail.com
  */
object SmartTableUtil {

  /* Read Table Smart Basic from a pre-compressed file */
  def readSmartBasicTable(x: Array[CPIntVar], filePath: String): Array[Array[BasicSmartElement]] = {
    val smartTable = readCompressTable(filePath)
    val tableLength = smartTable.length
    val tableBasic = Array.fill(tableLength)(new Array[BasicSmartElement](x.length))

    for (i <- smartTable.indices; j <- x.indices) {
      var smartElement: BasicSmartElement = null

      val element = smartTable(i)(j).trim().split(" ")
      if (element.head == "*") {
        smartElement = Star()
      } else {
        val value = element.last.toInt
        if (element.head == "=") {
          smartElement = Equal(value)
        } else if (element.head == "[") {
          smartElement = LessEq(value)
        } else if (element.head == "]") {
          smartElement = GreatEq(value)
        } else if (element.head == "#") {
          smartElement = NotEqual(value)
        } else {
          println("ERROR !!!!")
        }
      }
      tableBasic(i)(j) = smartElement
    }
    tableBasic
  }

  def getCTStarFromFile(x:Array[CPIntVar], filePath: String): Constraint = {
    val arity = x.length
    val compressTuples = readCompressTable(filePath)

    // map to table star with star value is -1
    val starTuples = ArrayBuffer[Array[Int]]()
    for (compressTuple <- compressTuples) {
      val tuple = Array.fill(arity)(-1)
      for (i <- 0 until arity) {
        val restriction = compressTuple(i).trim
        if (restriction != "*") {
          val element = restriction.split(" ")
          if (element(0) == "=") {
            tuple(i) = element(1).toInt
          } else {
            println("ERROR !!!!")
          }
        } else {
          tuple(i) = -1
        }
      }
      starTuples += tuple
    }

    new TableCTStar(x, starTuples.toArray)
  }

  def getSmartTableFromFile(x:Array[CPIntVar], tuples:Array[Array[Int]], filePath: String): Constraint = {
    val compressTuples = readCompressTable(filePath)
    createSmartTable(x, tuples, compressTuples)
  }

  def createSmartTable(x: Array[CPIntVar], validTuples:Array[Array[Int]], compressTuples: Array[Array[String]]): Constraint = {
    val tableSmart = new TableSmart(x, validTuples)
    val smartTuples = ArrayBuffer[SmartTuple]()

    for (compressTuple <- compressTuples) {
      val smartTuple = new SmartTuple(tableSmart)

      for (i <- x.indices) {
        val restriction = compressTuple(i).trim
        if (restriction != "*") {
          val element = restriction.split(" ")
          if (element(0) == "[")
            element(0) = "<="
          else if (element(0) == "]")
            element(0) = ">="
          else if (element(0) == "#")
            element(0) = "!="

          if (element(1).contains("x")) {
            // restriction in the form: op variable. Example: > x2
            smartTuple.addRestrictionBinary(i, element(0), element(1).substring(1).toInt)
          } else {
            // restriction in the form: op value. Example: < 5
            smartTuple.addRestrictionUnary(i, element(0), element(1).toInt)
          }
        }
      }
      smartTuples += smartTuple
    }
    tableSmart.storeTuples(smartTuples.toArray)
  }

  /* Read compressed tuples from a pre-compressed file of table constraint*/
  def readCompressTable(path: String): Array[Array[String]] = {
    val lines = Source.fromFile(path).getLines()
    val nbVars = lines.next().split(" ").head.toInt
    for (i <- 0 until nbVars) lines.next() // skip domain of variables

    val nbSmartTuples = lines.next().split(" ").head.toInt
    val compressTuples = ArrayBuffer[Array[String]]()
    for (i <- 0 until nbSmartTuples) {
      compressTuples += lines.next().split("\\|")
    }
    compressTuples.toArray
  }

}
