package oscar.thesis.tablesmart

import oscar.algo.Inconsistency
import oscar.cp._
import oscar.cp.constraints.tables.TableAlgo._
import oscar.cp.constraints.tables.{table, _}
import oscar.thesis.strhybrid.TableSTRHybrid

import scala.io.Source

/**
  *
  * Benchmark of different algorithms on pre-compressed table.
  * at each turn, 10% values will be removed and do propagation until a failure occur.
  */
object BenchTableRandomAll extends App {
  var tableIns = args(0)
  val algo = args(1)

  val tableAlgos = Map(
    "ct" -> ((x: Array[CPIntVar], tuples: Array[Array[Int]]) => new TableCT(x, tuples)),
    "str2" -> ((x: Array[CPIntVar], tuples: Array[Array[Int]]) => new TableSTR2(x, tuples)),
    "strhybrid" -> ((x: Array[CPIntVar], tuples: Array[Array[Int]]) => new TableSTRHybrid(x, tuples)),
    "mdd" -> ((x: Array[CPIntVar], tuples: Array[Array[Int]]) => table(x, tuples,MDD4R)),
    "ctbasic" -> ((x: Array[CPIntVar], tuples: Array[Array[Int]]) => TableCTBs(x, tuples))
  )

  /* Read table instance */
  val tt0 = System.currentTimeMillis()
  val (nVariables, domains, tuples) = readTable(tableIns)
  val tt1 = System.currentTimeMillis()


  implicit val cp = CPSolver()
  val x: Array[CPIntVar] = Array.tabulate(nVariables)(i => CPIntVar(domains(i), "x" + i))
  val random = cp.random


  /* Preprocessing + setup constraint */
  val tt2 = System.currentTimeMillis()

  var tableConstraint: Constraint = _

  if (algo == "st"){
    tableConstraint = SmartTableUtil.getSmartTableFromFile(x, tuples, tableIns + ".all")
  } else if (algo == "ststar") {
    tableConstraint = SmartTableUtil.getSmartTableFromFile(x, tuples, tableIns + ".str")
  } else if (algo == "ctstar") {
    tableConstraint = SmartTableUtil.getCTStarFromFile(x, tableIns + ".str")
  } else if (algo == "ctbasic") {
    tableConstraint = new TableCTBs(x, SmartTableUtil.readSmartBasicTable(x, tableIns + ".val"))
  } else {
    tableConstraint = tableAlgos(algo)(x, tuples)
  }
  tableConstraint.setup(Strong)


  //---------------------------------
  val tt3 = System.currentTimeMillis()

  /* Running benchmark with first protocol: in the top of the tree */
  var propagationTime = 0L
  var restorationTime = 0L

  val tt4 = System.currentTimeMillis()

  val nIterations = 1000
  var i = 0
  while (i < nIterations) {
    cp.pushState() // Store state before removal

    var failed = false
    while (!failed) {
      val nTotalValues = x.map(_.size).sum
      val nRemove = 1 + 10 * nTotalValues / 100

      /* Remove 10% values in variable domains */
      var cnt = 0
      while (cnt < nRemove && !failed) {
        val varId = random.nextInt(nVariables)
        if (x(varId).size == 1) {
          failed = true
        } else {
          val value = x(varId).randomValue(random)
          x(varId).removeValue(value)
          cnt += 1
        }
      }

      /* Run propagation */
      //val t0 = System.currentTimeMillis()
      val t0 = System.nanoTime()
      if (!failed) {
        try {
          tableConstraint.execute()
        } catch {
          case e: Inconsistency =>
            failed = true
        }
      }
      //val t1 = System.currentTimeMillis()
      val t1 = System.nanoTime()

      propagationTime += t1 - t0
      //restorationTime += t2 - t1
    }


    cp.pop() // Restore state before removal
    i += 1
  }
  propagationTime /= 1000000

  val tt5 = System.currentTimeMillis()

  val readInsTime = tt1 - tt0
  val preProcessTime = tt3 - tt2
  val benchmarkTime = tt5 - tt4

  var result = tableIns.split("\\/").last + s" $benchmarkTime $propagationTime $restorationTime $readInsTime $preProcessTime "
  if (algo == "st" || algo == "ststar") {
    result += s"${tableConstraint.asInstanceOf[TableSmart].getNbOrdinaryTuples} ${tableConstraint.asInstanceOf[TableSmart].getNbSmartTuples}"
  } else if (algo == "ctbasic") {
    // To avoid error: add "def getNbSmartTuples = table.length" in the class TableCTBs
    result += s"${tuples.size} ${tableConstraint.asInstanceOf[TableCTBs].getNbSmartTuples}"
  }
  println(result)



  //--------------------------
  // Read input
  def readTable(path: String, toReverse: Boolean = false) = {
    val lines = Source.fromFile(path).getLines()

    val variableLine = lines.next()
    val nbVariables = variableLine.split(" ")(0).toInt
    val domains = Array.fill(nbVariables)(Array[Int]())

    for (i <- 0 until nbVariables) {
      val line = lines.next()
      if (line.contains("..")) { // domain is given by an interval min..max
      val interval = line.split(" ")(1).split("\\.\\.").map(_.toInt)
        val dom = (interval(0) to interval(1)).toArray
        domains(i) = dom
      } else { // domain is given by listing all values
      val dom = line.split(" ").drop(1).map(_.toInt)
        domains(i) = dom
      }
    }

    val tupleLine = lines.next()
    val nbTuples = tupleLine.split(" ")(0).toInt
    val tuples: Array[Array[Int]] = Array.fill(nbTuples)(Array[Int]())

    for (i <- 0 until nbTuples) {
      val line = lines.next()
      val tuple = line.split(" ").map(_.toInt)
      tuples(i) = tuple
    }

    if (!toReverse)
      (nbVariables, domains, tuples)
    else
      (nbVariables, domains.reverse, tuples.map(_.reverse))
  }

}
