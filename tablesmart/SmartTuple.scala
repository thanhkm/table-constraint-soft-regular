package oscar.thesis.tablesmart

import oscar.cp.core.variables.CPIntVar

import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer

/**
  * Implementation of smart table constraint
  *      J.B. Mairy, Y. Deville, C. Lecoutre, The Smart Table Constraint. CPAIOR 2015.
  * @author: thanhkm thanhkhongminh@gmail.com
  */
class SmartTuple(constraint: TableSmart) {
  private[this] var name = "smartTuple"
  private[this] val vars = constraint.vars
  private[this] val maxDomSize = vars.map(_.size).max
  private[this] val arity = vars.length
  // base tuple
  private[this] val STAR = Tuple.STAR
  private[this] val baseTuple = Array.fill(arity)(STAR)

  //---------------------------
  // For filtering general tree
  private[this] val domCopy = Array.tabulate(arity)(i => new SparseSet(vars(i).min, vars(i).max, vars(i).size))
  private[this] val tmpDom = Array.ofDim[Int](vars.map(_.size).max)
  private[this] var nTmpDom = 0
  //---------------------------

  // values that are not proven to GAC yet
  private[this] val supportless = constraint.supportless

  private[this] var supTime = 0L
  private[this] var valTime = 0L

  // minor constraint in this smart Tuple
  private[this] var restrictions: Array[Restriction] = Array()
  private[this] val collectedRestrictions: ArrayBuffer[Restriction] = ArrayBuffer[Restriction]()
  private[this] val whichRestrictions = Array.fill[Restriction](arity)(null)

  def tupleConstraints = whichRestrictions

  //-------- using for checking acyclicity ------------
  private[this] val parent = Array.fill(arity)(-1)
  // Find the subset of an element i
  private[this] def find(parent: Array[Int], i: Int): Int = {
    if (parent(i) == -1) i
    else find(parent, parent(i))
  }
  // Union of two subsets
  private[this] def union(parent: Array[Int], x: Int, y: Int) = {
    val xSet = find(parent, x)
    val ySet = find(parent, y)
    parent(xSet) = ySet
  }
  // Return true if exist cycle
  private[this] def unionAndCheckCycle(x: Int, y:Int): Boolean = {
    val xParent = find(parent, x)
    val yParent = find(parent, y)

    if (xParent == yParent) true
    else {
      union(parent, xParent, yParent)
      false
    }
  }

  def addRestrictionUnary(xId: Int, op: String, value: Int): SmartTuple = {
    var res: Restriction = null
    if (op.equals("!=")) {
      res = new RestrictionUnaryNe(xId, value)
    } else if (op.equals("=")) {
      res = new RestrictionUnaryEq(xId, value)
    } else if (op.equals("<")) {
      res = new RestrictionUnaryLE(xId, value - 1)
    } else if (op.equals("<=")) {
      res = new RestrictionUnaryLE(xId, value)
    } else if (op.equals(">")) {
      res = new RestrictionUnaryGE(xId, value + 1)
    } else if (op.equals(">=")) {
      res = new RestrictionUnaryGE(xId, value)
    } else {
      throw new Exception("non defined operator")
    }
    // add restriction
    collectedRestrictions += res

    this
  }

  def addRestrictionBinary(xId: Int, op: String, yId: Int): SmartTuple = {
    if (unionAndCheckCycle(xId, yId))
      throw new Exception("Non-acyclic smart tuple is not handle yet")

    var res: Restriction = null
    if (op.equals("!=")) {
      res = new RestrictionBinaryNe(xId, yId)
    } else if (op.equals("=")) {
      res = new RestrictionBinaryEq(xId, yId)
    } else if (op.equals("<")) {
      res = new RestrictionBinaryL(xId, yId, true)
    } else if (op.equals("<=")) {
      res = new RestrictionBinaryL(xId, yId, false)
    } else if (op.equals(">")) {
      res = new RestrictionBinaryG(xId, yId, true)
    } else if (op.equals(">=")) {
      res = new RestrictionBinaryG(xId, yId, false)
    } else {
      throw new Exception("non defined operator")
    }
    // add restriction
    collectedRestrictions += res

    this
  }

  def setup(): Unit = {
    val ccRestrictions = computeConnectedRestrictions()
    for (ccRes <- ccRestrictions) {
      val r = buildRestriction(ccRes)
      restrictions ++= Array(r)
    }

    for (res <- restrictions) {
      whichRestrictions(res.xId) = res
      if (res.isInstanceOf[RestrictionBinary])
        whichRestrictions(res.asInstanceOf[RestrictionBinary].yId) = res
      else if (res.isInstanceOf[RestrictionMultiple]) {
        for (r <- res.asInstanceOf[RestrictionMultiple].involvedRestrictions) {
          if (r.isInstanceOf[RestrictionBinary])
            whichRestrictions(r.asInstanceOf[RestrictionBinary].yId) = res
        }
      } else if (res.isInstanceOf[RestrictionTree]) {
        val allVars = res.asInstanceOf[RestrictionTree].allVars
        for (varId <- allVars) {
          whichRestrictions(varId) = res
        }
      }
    }
    name = toString
  }

  private[this] def buildRestriction(ccRes: Array[Restriction]): Restriction = {
    var restriction: Restriction = null
    if (ccRes.length == 1) {
      //println("build restriction simple")
      restriction = ccRes(0)
    } else {
      val nbRes = Array.fill(arity)(0) // number of restrictions involved with variable
      for (r <- ccRes) {
        nbRes(r.xId) += 1
        if (r.isInstanceOf[RestrictionBinary])
          nbRes(r.asInstanceOf[RestrictionBinary].yId) += 1
      }
      var isRestrictionMultiple = false
      var varId = arity
      while (varId > 0 && !isRestrictionMultiple) {
        varId -= 1
        if (nbRes(varId) == ccRes.length) {
          isRestrictionMultiple = true
        }
      }

      // Build restriction multiple or restriction tree
      if (isRestrictionMultiple) {
        //println("build multiple tree")
        val rr = ArrayBuffer[Restriction]()
        for (res <- ccRes) {
          if (res.xId == varId) { // varId is first variable
            rr += res
          } else { // varId is second variable, need to reverse
            rr += reverseRestriction(res.asInstanceOf[RestrictionBinary])
          }
        }
        restriction = new RestrictionMultiple(varId, rr.toArray)

      } else {
        //println("build restriction tree")
        restriction = buildRestrictionTree(ccRes)
        //throw new Exception("RestrictionTree is in development")
      }
    }

    restriction
  }

  private[this] def buildRestrictionTree(ccRes: Array[Restriction]): Restriction = {
    val rstr1 = Array.fill(arity)(ArrayBuffer[Restriction]()) // rstr1(xId) restrictions with xId as first variable
    val rstr2 = Array.fill(arity)(ArrayBuffer[Restriction]()) // rstr2(xId) restrictions with xId as second variable
    for (res <- ccRes) {
      rstr1(res.xId) += res
      if (res.isInstanceOf[RestrictionBinary])
        rstr2(res.asInstanceOf[RestrictionBinary].yId) += res
    }

    val visitedVar = Array.fill(arity)(false)
    val unvisitedRestrSet = mutable.Set[Restriction]() ++ ccRes
    val branches = ArrayBuffer.fill(arity)(mutable.Set[Restriction]())
    def dfs(varId: Int, depth: Int): Unit = {
      for (res <- rstr1(varId)) {
        if (unvisitedRestrSet.contains(res)) {
          branches(depth) += res
          unvisitedRestrSet.remove(res)
        }
        if (res.isInstanceOf[RestrictionBinary]) {
          val otherVar = res.asInstanceOf[RestrictionBinary].yId
          if (!visitedVar(otherVar)) {
            visitedVar(otherVar) = true
            dfs(otherVar, depth + 1)
          }
        }
      }
      for (res <- rstr2(varId)) {
        val otherVar = res.xId
        if (!visitedVar(otherVar)) {
          unvisitedRestrSet.remove(res)
          branches(depth) += reverseRestriction(res.asInstanceOf[RestrictionBinary])

          visitedVar(otherVar) = true
          dfs(otherVar, depth+1)
        }
      }
    }

    visitedVar(ccRes(0).xId) = true
    dfs(ccRes(0).xId, 0)

    assert(unvisitedRestrSet.isEmpty, "All restriction must be visited.")
    assert(ccRes(0).xId == branches(0).head.xId, "First variable must be the same.")

    val varSet = mutable.HashSet[Int]()
    for (res <- ccRes) {
      varSet += res.xId
      if (res.isInstanceOf[RestrictionBinary])
        varSet += res.asInstanceOf[RestrictionBinary].yId
    }

    new RestrictionTree(ccRes(0).xId, varSet.toArray, branches.filter(_.nonEmpty).map(_.toArray).toArray)
  }

  private[this] def reverseRestriction(resBinary: RestrictionBinary): RestrictionBinary = {
    var res: RestrictionBinary = null
    val xId = resBinary.xId
    val yId = resBinary.yId

    if (resBinary.isInstanceOf[RestrictionBinaryEq]) {
      res = new RestrictionBinaryEq(yId, xId)
    } else if (resBinary.isInstanceOf[RestrictionBinaryNe]) {
      res = new RestrictionBinaryNe(yId, xId)
    } else if (resBinary.isInstanceOf[RestrictionBinaryG]) {
      val strict = resBinary.asInstanceOf[RestrictionBinaryG].strict
      res = new RestrictionBinaryL(yId, xId, strict)
    } else if (resBinary.isInstanceOf[RestrictionBinaryL]) {
      val strict = resBinary.asInstanceOf[RestrictionBinaryL].strict
      res = new RestrictionBinaryG(yId, xId, strict)
    } else {
      throw new Exception("Restriction not handle yet.")
    }

    res
  }

  private[this] def computeConnectedRestrictions(): Array[Array[Restriction]] = {
    val rstr1 = Array.fill(arity)(ArrayBuffer[Restriction]()) // rstr1(xId) restrictions with xId as first variable
    val rstr2 = Array.fill(arity)(ArrayBuffer[Restriction]()) // rstr2(xId) restrictions with xId as second variable
    for (res <- collectedRestrictions) {
      rstr1(res.xId) += res
      if (res.isInstanceOf[RestrictionBinary])
        rstr2(res.asInstanceOf[RestrictionBinary].yId) += res
    }

    val visited = Array.fill(arity)(false)

    def dfs(varId: Int, set: mutable.Set[Restriction]): Unit = {
      for (res <- rstr1(varId)) {
        set += res
        if (res.isInstanceOf[RestrictionBinary]) {
          val otherVar = res.asInstanceOf[RestrictionBinary].yId
          if (!visited(otherVar)) {
            visited(otherVar) = true
            dfs(otherVar, set)
          }
        }
      }

      for (res <- rstr2(varId)) {
        set += res
        val otherVar = res.xId
        if (!visited(otherVar)) {
          visited(otherVar) = true
          dfs(otherVar, set)
        }
      }
    }

    val ccRestrictions = ArrayBuffer[mutable.Set[Restriction]]() // connected componant restrictions
    for (varId <- 0 until arity) {
      if (!visited(varId)) {
        val set = mutable.HashSet[Restriction]()
        visited(varId) = true
        dfs(varId, set)
        if (set.nonEmpty) ccRestrictions += set
      }
    }

    ccRestrictions.map(_.toArray).toArray
  }


  @inline def isValid(sVal: Array[Int], sValSize: Int): Boolean = {
    valTime += 1

    var i = 0
    while (i < sValSize) {
      val varId = sVal(i)
      if (whichRestrictions(varId) == null) {
        if (baseTuple(varId) != STAR && !vars(varId).hasValue(baseTuple(varId)))
          return false
      } else
      if (whichRestrictions(varId).valTimeLocal != valTime)
        if (!whichRestrictions(varId).isValid()) return false
      i += 1
    }

    true
  }

  @inline def collect(sSup: Array[Int], sSupSize: Int): Int = {
    supTime += 1

    var sSupSizeLocal = sSupSize
    var i = sSupSize - 1
    while (i >= 0) {
      val varId = sSup(i)
      if (supportless(varId).isEmpty()) {
        sSupSizeLocal -= 1
        sSup(i) = sSup(sSupSizeLocal)
      } else {
        val restriction = whichRestrictions(varId)
        if (restriction == null) {
          if (baseTuple(varId) == STAR)
            supportless(varId).reset()
          else
            supportless(varId).remove(baseTuple(varId))
        } else {
          if (restriction.supTimeLocal != supTime) {
            if (restriction.valTimeLocal != valTime && (restriction.isInstanceOf[RestrictionMultiple] || restriction.isInstanceOf[RestrictionTree]))
              restriction.isValid()
            restriction.collect()
          }
        }
      }
      i -= 1
    }

    sSupSizeLocal
  }

  override def toString: String = {
    var res = ""
    for (i <- 0 until arity) {
      if (whichRestrictions(i) == null)
        if (baseTuple(i) == STAR)
          res += "  *   |"
        else
          res += " =  " + baseTuple(i) + " |"
      else {
        if  (i != whichRestrictions(i).xId)
          res += "-->x" + whichRestrictions(i).xId + " |"
        else
          res += whichRestrictions(i) + " |"
      }
    }
    res
  }

  /** **************************************************************/
  /** **************************************************************/
  /** **************************************************************/
  abstract class Restriction(val xId: Int) {
    // dom
    val x: CPIntVar = vars(xId)
    val offsetX = constraint.offsets(xId)
    // supportless
    val slX = supportless(xId)
    // using to avoid duplicate valid or collect
    var valTimeLocal = 0L
    var supTimeLocal = 0L

    //def checkTuple(tuple: Array[Int]): Boolean
    def isValid(): Boolean

    // To implement
    def isValidFor(value: Int): Boolean

    def collect(): Unit

    override def toString: String = {
      xId + " "
    }
  }

  /** **************************************************************/
  /** **************************************************************/
  abstract class RestrictionUnary(override val xId: Int, val value: Int) extends Restriction(xId) {

    def filterX(domX: SparseSet): Unit

    override def toString: String = {
      xId + " " + x + " " + value
    }
  }

  /**
    * x = value
    */
  class RestrictionUnaryEq(override val xId: Int, override val value: Int) extends RestrictionUnary(xId, value) {

    override def isValidFor(value: Int): Boolean = {
      value == this.value //&& isValid()
    }

    override def isValid(): Boolean = {
      x.hasValue(value)
    }

    override def collect(): Unit = {
      slX.remove(value)
    }

    override def filterX(domX: SparseSet): Unit = {
      if (domX.contains(value)) domX.resetTo(value)
      else domX.reset()
    }

    override def toString: String = {
      " = " + value + " "
    }
  }

  /**
    * x != value
    */
  class RestrictionUnaryNe(override val xId: Int, override val value: Int) extends RestrictionUnary(xId, value) {
    override def isValidFor(value: Int): Boolean = {
      value != this.value //&& x.hasValue(value)
    }

    override def isValid(): Boolean = {
      x.size > 1 || !x.hasValue(value)
    }

    override def collect(): Unit = {
      if (slX.contains(value)) slX.resetTo(value) // TODO corrected bug here
      else slX.reset()
    }

    override def filterX(domX: SparseSet): Unit = {
      domX.remove(value)
    }

    override def toString: String = {
      " !=" + value + " "
    }
  }

  /**
    * x <= value
    */
  class RestrictionUnaryLE(override val xId: Int, override val value: Int) extends RestrictionUnary(xId, value) {
    override def isValidFor(value: Int): Boolean = {
      value <= this.value //&& x.hasValue(value)
    }

    override def isValid(): Boolean = {
      x.min <= value
    }

    override def collect(): Unit = {
//      val ite = x.iterator
//      while (ite.hasNext) {
//        val v = ite.next()
//        if (v <= value)
//          slX.remove(v)
//      }

      nTmpDom = x.fillArray(tmpDom)
      var i = 0
      while (i < nTmpDom) {
        val v = tmpDom(i)
        if (v <= value)
          slX.remove(v)
        i += 1
      }

    }

    override def filterX(domX: SparseSet): Unit = {
      nTmpDom = domX.fillArray(tmpDom)
      var i = 0
      while (i < nTmpDom) {
        val v = tmpDom(i)
        if (v > value) domX.remove(v)
        i += 1
      }
    }

    override def toString: String = {
      " <=" + value + " "
    }
  }

  /**
    * x >= value
    */
  class RestrictionUnaryGE(override val xId: Int, override val value: Int) extends RestrictionUnary(xId, value) {
    override def isValidFor(value: Int): Boolean = {
      value >= this.value //&& x.hasValue(value)
    }

    override def isValid(): Boolean = {
      x.max >= value
    }

    override def collect(): Unit = {
      val ite = x.iterator
      while (ite.hasNext) {
        val v = ite.next()
        if (v >= value)
          slX.remove(v)
      }
    }

    override def filterX(domX: SparseSet): Unit = {
      nTmpDom = domX.fillArray(tmpDom)
      var i = 0
      while (i < nTmpDom) {
        val v = tmpDom(i)
        if (v < value) domX.remove(v)
        i += 1
      }

    }

    override def toString: String = {
      " >=" + value + " "
    }
  }

  /** **************************************************************/
  /** **************************************************************/
  abstract class RestrictionBinary(override val xId: Int, val yId: Int) extends Restriction(xId) {
    protected var nbVals = 0
    protected val vals = Array.ofDim[Int](x.size)

    val y = vars(yId)
    val offsetY = constraint.offsets(yId)
    val slY = supportless(yId)

    def collectY(domX: Array[Int], nDomX: Int): Unit

    def filterX(domX: SparseSet, domY: SparseSet): Unit
    def filterY(domX: SparseSet, domY: SparseSet): Unit

    override def toString: String = {
      xId + " " + yId
    }
  }

  /**
    * x = y
    */
  class RestrictionBinaryEq(override val xId: Int, override val yId: Int) extends RestrictionBinary(xId, yId) {
    private var residue = Tuple.STAR

    override def isValidFor(value: Int): Boolean = {
      y.hasValue(value) // && x.hasValue(value)
    }

    override def isValid(): Boolean = {
      valTimeLocal = valTime

      if (residue != Tuple.STAR && x.hasValue(residue) && y.hasValue(residue))
        return true

      val smallVar = if (x.size < y.size) x else y
      val bigVar = if (x.size < y.size) y else x
      val ite = smallVar.iterator
      while (ite.hasNext) {
        val v = ite.next()
        if (bigVar.hasValue(v)) {
          residue = v
          return true
        }
      }
      false
    }

    override def collect(): Unit = {
      supTimeLocal = supTime
      nbVals = 0
      val smallVar = if (x.getSize < y.getSize) x else y
      val bigVar = if (x.getSize < y.getSize) y else x
      val ite = smallVar.iterator
      while (ite.hasNext) {
        val v = ite.next()
        if (bigVar.hasValue(v)) {
          vals(nbVals) = v
          nbVals += 1
        }
      }

//      nbVals = 0
//      var i = slX.size()
//      while (i > 0) {
//        i -= 1
//        val v = slX(i)
//        if (y.hasValue(v)) {
//          vals(nbVals) = v
//          nbVals += 1
//        }
//      }

      // remove from supportless
      if (!slX.isEmpty())
        slX.remove(vals, nbVals)
      if (!slY.isEmpty())
        slY.remove(vals, nbVals)
    }

    override def collectY(domX: Array[Int], nDomX: Int): Unit = {
      // tmp contains all valid value from X
      // x = y means that all values in dom is also valid for y
      if (!slY.isEmpty())
        slY.remove(domX, nDomX)
    }

    override def filterX(domX: SparseSet, domY: SparseSet): Unit = {
      nTmpDom = domX.fillArray(tmpDom)
      var i = 0
      while (i < nTmpDom) {
        val v = tmpDom(i)
        if (!domY.contains(v)) domX.remove(v)
        i += 1
      }
    }

    def filterY(domX: SparseSet, domY: SparseSet): Unit = {
      nTmpDom = domY.fillArray(tmpDom)
      var i = 0
      while (i < nTmpDom) {
        val v = tmpDom(i)
        if (!domX.contains(v)) domY.remove(v)
        i += 1
      }
    }

    override def toString: String = {
      " = x" + yId
    }
  }

  /**
    * x != y
    */
  class RestrictionBinaryNe(override val xId: Int, override val yId: Int) extends RestrictionBinary(xId, yId) {

    override def isValidFor(value: Int): Boolean = {
      (y.size > 1 || value != y.min) //&& x.hasValue(value)
    }

    override def isValid(): Boolean = {
      valTimeLocal = valTime

      x.size > 1 || y.size > 1 || x.min != y.min
    }

    override def collect(): Unit = {
      supTimeLocal = supTime

      if (x.size > 1) {
        slY.reset()
        if (y.size == 1 && slX.contains(y.min))
          slX.resetTo(y.min)
        else slX.reset()
      } else {
        // x.size == 1
        slX.reset()
        if (slY.contains(x.min)) slY.resetTo(x.min)
        else slY.reset()
      }
    }

    override def collectY(domX: Array[Int], nDomX: Int): Unit = {
      // tmp contains all valid value from X
      if (nDomX == 1 && slY.contains(domX(0)))
        slY.resetTo(domX(0))
      else slY.reset()
    }


    override def filterX(domX: SparseSet, domY: SparseSet): Unit = {
      if (domY.size() == 1)
        domX.remove(domY.firstValue()) // TODO recheck
    }

    def filterY(domX: SparseSet, domY: SparseSet): Unit = {
      if (domX.size() == 1)
        domY.remove(domX.firstValue()) // TODO recheck
    }

    override def toString: String = {
      " !=x" + yId
    }
  }

  /**
    * x <(=) y
    */
  class RestrictionBinaryL(override val xId: Int, override val yId: Int, val strict: Boolean) extends RestrictionBinary(xId, yId) {

    override def isValidFor(value: Int): Boolean = {
      if (strict) value < y.max
      else value <= y.max
    }

    override def isValid(): Boolean = {
      valTimeLocal = valTime

      if (strict) x.min < y.max
      else x.min <= y.max
    }

    override def collect(): Unit = {
      supTimeLocal = supTime

      var ite = x.iterator
      while (ite.hasNext) {
        val v = ite.next()
        if (isValidFor(v)) {
          slX.remove(v)
        }
      }

      ite = y.iterator
      while (ite.hasNext) {
        val v = ite.next()
        if ((strict && v > x.min) || (!strict && v >= x.min)) {
          slY.remove(v)
        }
      }

//      nbVals = 0
//      var i = slX.size()
//      while (i > 0) {
//        i -= 1
//        val v = slX(i)
//        if (isValidFor(v)) {
//          vals(nbVals) = v
//          nbVals += 1
//        }
//      }
//      slX.remove(vals, nbVals)
//
//      nbVals = 0
//      i = slY.size()
//      while (i > 0) {
//        i -= 1
//        val v = slY(i)
//        if ((strict && v > x.min) || (!strict && v >= x.min)) {
//          vals(nbVals) = v
//          nbVals +=1
//        }
//      }
//      slY.remove(vals, nbVals)

    }

    override def collectY(domX: Array[Int], nDomX: Int): Unit = {
      if (!slY.isEmpty()) {
        var xmin = domX(0) // TODO bug here
        var i = 1
        while (i < nDomX) {
          xmin = math.min(xmin, domX(i))
          i += 1
        }

        val ite = y.iterator
        while (ite.hasNext) {
          val v = ite.next()
          if ((strict && v > xmin) || (!strict && v >= xmin)) {
            slY.remove(v)
          }
        }
      }
    }

    // TODO: debug here
    override def filterX(domX: SparseSet, domY: SparseSet): Unit = {
      nTmpDom = domY.fillArray(tmpDom)
      var ymax = tmpDom(0)
      var i = 1
      while (i < nTmpDom) {
        ymax = math.max(tmpDom(i), ymax)
        i += 1
      }

      ymax = if (strict) ymax-1 else ymax
      nTmpDom = domX.fillArray(tmpDom)
      i = 0
      while (i < nTmpDom) {
        val v = tmpDom(i)
        if (v > ymax) domX.remove(v)
        i+= 1
      }
    }

    def filterY(domX: SparseSet, domY: SparseSet): Unit = {
      nTmpDom = domX.fillArray(tmpDom)
      var xmin = tmpDom(0)
      var i = 1
      while (i < nTmpDom) {
        xmin = math.min(tmpDom(i), xmin)
        i += 1
      }

      xmin = if (strict) xmin+1 else xmin

      nTmpDom = domY.fillArray(tmpDom)
      i = 0
      while (i < nTmpDom) {
        val v = tmpDom(i)
        if (v < xmin) domY.remove(v)
        i+= 1
      }
    }

    override def toString: String = {
      if (strict) " < x" + yId
      else " <=x" + yId
    }
  }

  /**
    * x >(=) y
    */
  class RestrictionBinaryG(override val xId: Int, override val yId: Int, val strict: Boolean) extends RestrictionBinary(xId, yId) {

    override def isValidFor(value: Int): Boolean = {
      if (strict) value > y.min
      else value >= y.min
    }

    override def isValid(): Boolean = {
      valTimeLocal = valTime
      if (strict) x.max > y.min
      else x.max >= y.min
    }

    override def collect(): Unit = {
      supTimeLocal = supTime

      var ite = x.iterator
      while (ite.hasNext) {
        val v = ite.next()
        if (isValidFor(v)) {
          slX.remove(v)
        }
      }

      ite = y.iterator
      while (ite.hasNext) {
        val v = ite.next()
        if ((strict && v < x.max) || (!strict && v <= x.max)) {
          slY.remove(v)
        }
      }
    }

    override def collectY(domX: Array[Int], nDomX: Int): Unit = {
      if (!slY.isEmpty()) {

        var xmax = domX(0) // TODO verify max here
        var i = 1
        while (i < nDomX) {
          xmax = math.max(xmax, domX(i))
          i += 1
        }

        val ite = y.iterator
        while (ite.hasNext) {
          val v = ite.next()
          if ((strict && v < xmax) || (!strict && v <= xmax)) {
            slY.remove(v)
          }
        }
      }
    }

    // TODO: Verify here
    override def filterX(domX: SparseSet, domY: SparseSet): Unit = {
      nTmpDom = domY.fillArray(tmpDom)
      var ymin = tmpDom(0)
      var i = 1
      while (i < nTmpDom) {
        ymin = math.min(tmpDom(i), ymin)
        i += 1
      }

      ymin = if (strict) ymin+1 else ymin

      nTmpDom = domX.fillArray(tmpDom)
      i = 0
      while (i < nTmpDom) {
        val v = tmpDom(i)
        if (v < ymin) domX.remove(v)
        i+= 1
      }

    }

    def filterY(domX: SparseSet, domY: SparseSet): Unit = {
      nTmpDom = domX.fillArray(tmpDom)
      var xmax = tmpDom(0)
      var i = 1
      while (i < nTmpDom) {
        xmax = math.max(tmpDom(i), xmax)
        i += 1
      }

      xmax = if (strict) xmax-1 else xmax

      nTmpDom = domY.fillArray(tmpDom)
      i = 0
      while (i < nTmpDom) {
        val v = tmpDom(i)
        if (v > xmax) domY.remove(v)
        i+= 1
      }
    }

    override def toString: String = {
      if (strict) " > x" + yId
      else " >=x" + yId
    }
  }


  class RestrictionMultiple(override val xId: Int, val involvedRestrictions: Array[Restriction]) extends Restriction(xId) {
    private val resBinaries = involvedRestrictions.filter(res => res.isInstanceOf[RestrictionBinary]).map(_.asInstanceOf[RestrictionBinary])
    // Array contains values for propagation
    val domX = Array.ofDim[Int](vars(xId).size)
    // nb of values present in dom
    var nDomX = 0

    override def isValidFor(value: Int): Boolean = {
      var i = involvedRestrictions.length
      while (i > 0) {
        i -= 1
        if (!involvedRestrictions(i).isValidFor(value))
          return false
      }
      true
    }

    override def isValid(): Boolean = {
      valTimeLocal = valTime

      nDomX = 0
      val iter = x.iterator
      while (iter.hasNext) {
        val v = iter.next()
        if (isValidFor(v)) {
          domX(nDomX) = v
          nDomX += 1
        }
      }
      nDomX > 0
    }

    override def collect(): Unit = {
      supTimeLocal = supTime
      slX.remove(domX, nDomX)

      if (nDomX == 0) {
        println("error for: " + this + "\n " + name)
        println(vars.map(_.mkString(" ")).mkString(" | "))
      }

      var i = resBinaries.length
      while (i > 0) {
        i -= 1
        resBinaries(i).collectY(domX, nDomX)
      }

    }

    override def toString: String = "x" + xId + involvedRestrictions.mkString("")

  }

  class RestrictionTree(override val xId: Int, val allVars: Array[Int], val branches: Array[Array[Restriction]]) extends Restriction(xId) {
    val treeHeight = branches.length

    override def isValidFor(value: Int): Boolean = {
      ???
      true
    }

    override def isValid(): Boolean = {
      valTimeLocal = valTime

      var i = allVars.length
      while (i > 0) {
        i -= 1
        val varId = allVars(i)
        nTmpDom = vars(varId).fillArray(tmpDom)
        domCopy(varId).resetTo(tmpDom, nTmpDom)
      }

      i = treeHeight
      while (i > 0 ) {
        i -= 1

        var j = 0
        while (j < branches(i).length) {
        //for (res <- branches(i)) {
          val res = branches(i)(j)
          if (res.isInstanceOf[RestrictionUnary])
            res.asInstanceOf[RestrictionUnary].filterX(domCopy(res.xId))
          else {
            val yId = res.asInstanceOf[RestrictionBinary].yId
            if (domCopy(yId).isEmpty()) {
              return false
            }
            res.asInstanceOf[RestrictionBinary].filterX(domCopy(res.xId), domCopy(yId))
          }

          j+= 1
        }
      }

      !domCopy(xId).isEmpty()
    }

    override def collect(): Unit = {
      supTimeLocal = supTime
      var h = 0
      while (h < treeHeight) {
        var j = 0
        while (j < branches(h).length) {
        //for (res <- branches(h)) {
          val res = branches(h)(j)
          if (res.isInstanceOf[RestrictionBinary]) {
            val yId = res.asInstanceOf[RestrictionBinary].yId
            res.asInstanceOf[RestrictionBinary].filterY(domCopy(res.xId), domCopy(yId))
          }
          j += 1
        }
        h += 1
      }

      for (i <- allVars) {
        nTmpDom = domCopy(i).fillArray(tmpDom)
        supportless(i).remove(tmpDom, nTmpDom)
      }

    }

    override def toString: String = {
      branches.map(_.map(r => "x" + r.xId + r)).map(_.mkString(" ")).mkString(", ")
    }

  }

}


object Tuple {
  val STAR = Int.MaxValue
}
