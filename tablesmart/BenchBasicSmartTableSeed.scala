package oscar.thesis.tablesmart

import oscar.cp.core.CPSolver
import oscar.cp.core.variables.CPIntVar
import oscar.thesis.tablesmart.hel.GroundTable

import scala.io.Source
import scala.util.Random

/**
  * Class for compresseur benchmak
  *
  * @author ThanhKM thanhkhongminh@gmail.com
  *
  * Command for compression: programme instance algo seed
  */
object BenchBasicSmartTableSeed extends App {
  implicit val cp = new CPSolver()


  val tableIns = args(0)
  val algo = if (args.length > 1) args(1) else "bau"
  val seed = if (args.length > 2) args(2).toInt else 0

  val tableName = tableIns.split("\\/").last
  val (nVariables, domains, tuples) = readTable(tableIns)
  var listVar = (0 until nVariables).toList

  val random = new Random(seed)

  // shuffle variable
  listVar = if (seed == 0) listVar else if (seed == 9) listVar.reverse else random.shuffle(listVar)
  // get tuples and variables' domain following new order
  val shuffedTuples = Array.tabulate(tuples.length, nVariables)((i, j) => tuples(i)(listVar(j)))
  val shuffedDomains = Array.tabulate(nVariables)(i => domains(listVar(i)))

  // running benchmark
  print(listVar + " ")
  benchCompressor(algo, nVariables, shuffedDomains, shuffedTuples)


  //--------------------------
  def benchCompressor(algo:String, nVariables:Int, domains: Array[Array[Int]], tuples: Array[Array[Int]]): Unit = {
    if (algo == "bauAll") {
      val t0 = System.currentTimeMillis()
      val compressTuples = TableCompression.compressTuples(domains, tuples, "all")
      val t1 = System.currentTimeMillis()
      val runtime = t1-t0
      println(algo + " " + tableName + " " + runtime + " " + compressTuples.length + " " + tuples.length)

    } else if (algo == "bau") {
      val t0 = System.currentTimeMillis()
      val compressTuples = TableCompression.compressTuples(domains, tuples, "val")
      val t1 = System.currentTimeMillis()
      val runtime = t1-t0
      println(algo + " " + tableName + " " + runtime + " " + compressTuples.length + " " + tuples.length)

    } else if (algo == "hel") {
      val x: Array[CPIntVar] = Array.tabulate(nVariables)(i => CPIntVar(domains(i), "x" + i))
      val groundTable = new GroundTable(tuples, false) // le boolean dit si la table est déjà triée (true) ou pas triée (false)
      val t0 = System.currentTimeMillis()
      val basicSmartTable = groundTable.compressToBasicSmartTable(x) // compression de la table
      val t1 = System.currentTimeMillis()

      val runtime = t1 - t0
      println(algo + " " + tableName + " " + runtime + " " + basicSmartTable.getTable.length + " " + tuples.length)
    }
  }


  //--------------------------
  // Read input
  def readTable(path: String, toReverse: Boolean = false) = {
    val lines = Source.fromFile(path).getLines()

    val variableLine = lines.next()
    val nbVariables = variableLine.split(" ")(0).toInt
    val domains = Array.fill(nbVariables)(Array[Int]())

    for (i <- 0 until nbVariables) {
      val line = lines.next()
      if (line.contains("..")) { // domain is given by an interval min..max
      val interval = line.split(" ")(1).split("\\.\\.").map(_.toInt)
        val dom = (interval(0) to interval(1)).toArray
        domains(i) = dom
      } else { // domain is given by listing all values
      val dom = line.split(" ").drop(1).map(_.toInt)
        domains(i) = dom
      }
    }

    val tupleLine = lines.next()
    val nbTuples = tupleLine.split(" ")(0).toInt
    val tuples: Array[Array[Int]] = Array.fill(nbTuples)(Array[Int]())

    for (i <- 0 until nbTuples) {
      val line = lines.next()
      val tuple = line.split(" ").map(_.toInt)
      tuples(i) = tuple
    }

    if (!toReverse)
      (nbVariables, domains, tuples)
    else
      (nbVariables, domains.reverse, tuples.map(_.reverse))
  }

}
