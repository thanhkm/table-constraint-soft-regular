package oscar.thesis.tablesmart

import oscar.algo.Inconsistency
import oscar.algo.reversible.ReversibleInt
import oscar.cp.core.variables.{CPIntVar, CPVar}
import oscar.cp.core.{CPPropagStrength, CPStore, Constraint}

/**
  * Implementation of smart table constraint
  *      J.B. Mairy, Y. Deville, C. Lecoutre, The Smart Table Constraint. CPAIOR 2015.
  * @author: thanhkm thanhkhongminh@gmail.com
  */
class TableSmart(val vars: Array[CPIntVar], var tuples: Array[Array[Int]] = null ) extends Constraint(vars(0).store, "TableSmart") {
	idempotent = true
	priorityL2 = CPStore.MaxPriorityL2 - 1

  override def associatedVars(): Iterable[CPVar] = vars

	// nbVars
	private[tablesmart] val arity = vars.length
  // SSup, SVal are omitted for now
  private[this] val sSup = Array.tabulate(arity)(i => i)
  private[this] var sSupSize = arity
  private[this] val sVal = Array.tabulate(arity)(i => i)
  private[this] var sValSize = arity
  private[this] val lastSize = Array.tabulate(vars.length)(i => new ReversibleInt(s, -1))
  
  private[this] val values = Array.ofDim[Int](vars.map(_.size).max)
  // Sparse set for support less values
  private[tablesmart] val supportless = Array.tabulate(arity)(i => new SparseSet(vars(i).min, vars(i).max, vars(i).size))
  private[tablesmart] val offsets /* used for position index */ = Array.tabulate(arity)(i => vars(i).min)
  
  private[this] var smartTuples: Array[SmartTuple] = _
  private[this] var dense: Array[Int] = _
  private[this] var rLimit: ReversibleInt = _

  def getRate = (tuples.length - smartTuples.length) * 100.0 / tuples.length
  def getNbOrdinaryTuples = tuples.length
  def getNbSmartTuples = smartTuples.length

  def storeTuples(smartTuples: Array[SmartTuple]): Constraint = {
	  smartTuples.foreach(_.setup())
    this.smartTuples = smartTuples
    dense = Array.tabulate(smartTuples.length)(i => i)
    rLimit = new ReversibleInt(s, smartTuples.length - 1)
    this
  }
  
  override def setup(l: CPPropagStrength): Unit = {
    propagate()
    vars.filter(!_.isBound).foreach(_.callPropagateWhenDomainChanges(this))
  }
  
  override def propagate(): Unit = {

    // Initialize sVal, sSup
    initSValSSup()

    // Initialize support less
    initSupportless()

    // Traversal smartTuples
    // Use to avoid directly update rLimit
    var limit = rLimit.value
    var i = limit
    while (i >= 0) {
      val sTuple = smartTuples(dense(i))
      val valid = sTuple.isValid(sVal, sValSize)
      if (valid) {
        sSupSize = sTuple.collect(sSup, sSupSize)
      } else {
        swap(dense, i, limit)
        limit -= 1
      }
      i -= 1
    }

    if (limit == -1) throw Inconsistency
    else rLimit.setValue(limit)

    updateDomain()

  }
  
  /*****************************************************************************/
  /************************** Helper functions *********************************/
  def isValid(tau: Array[Int]): Boolean = {
    var i = 0
    while (i< arity) {
      if (!vars(i).hasValue(tau(i)))
        return false
      i += 1
    }
    true
  }

  /**
   * update domain for variables after propagate
   */
  @inline private def updateDomain(): Unit = {
    var i = 0
    while (i < sSupSize) {
      val varId = sSup(i)
      val variable = vars(varId)
      val nValues = supportless(varId).fillArray(values)
      // Remove value
      var j = 0
      while (j < nValues) {
        variable.removeValue(values(j))
        j +=1
      }
      // update lastSize for SVal
      lastSize(varId).setValue(variable.size)
      
      i += 1
    }
  }
  
  @inline private def initSValSSup(): Unit = {
    // Reset SSup, SVal
    sSupSize = 0
    sValSize = 0
    
    var varId = 0
    while (varId < arity) {
      // notGACSet, SSup
      if (vars(varId).size > 1) {
        // add variable's index to SSup
        sSup(sSupSize) = varId
    		sSupSize += 1
      }
      
      // SVal
      if (vars(varId).size != lastSize(varId).getValue) {
        //add variable's index to SVal
        sVal(sValSize) = varId
    		sValSize += 1

        lastSize(varId).setValue(vars(varId).size)
      }
      varId += 1
    }
  }
  
  @inline private def initSupportless(): Unit = {
    var varId = 0
    while (varId < arity) {
    	val size = vars(varId).fillArray(values)
			supportless(varId).setValues(values, size)
			varId += 1
    }
  }
  
  @inline private def swap(array: Array[Int], idx1: Int, idx2: Int) = {
    val tmp = array(idx1)
    array(idx1) = array(idx2)
    array(idx2) = tmp
  }

  override def toString: String = {
    var res = (0 until arity).map(i => " x" + i + "   |").mkString("") + "\n"
    smartTuples.foreach { st => res += st.toString() + "\n" }
    res
  }
  
}

/*********************************************************/
/*********************************************************/
class SparseSet(val min: Int, val max: Int, capacity: Int) {
  private val values = new Array[Int](capacity)
  private val indexes = new Array[Int](max-min+1)
  private var _size = 0

  def apply(i: Int): Int = {
    values(i)
  }

  def size(): Int = {
    _size
  }
  
  def isEmpty(): Boolean = {
    _size == 0
  }
  
  def setValues(vals: Array[Int], nbVals: Int): Unit = {
    var i = 0
    while (i < nbVals) {
      values(i) = vals(i)
      indexes(values(i) - min) = i
      i += 1
    }

    _size = nbVals
  }
  
  def contains(value: Int): Boolean = {
    // index out of bound
    if (value > max || value < min)
      return false
    val idx = indexes(value - min)
    idx < _size && values(idx) == value
  }
  
  def add(value: Int): Unit = {
    if (!contains(value)) {
      values(_size) = value
      indexes(value - min) = _size
      _size += 1
    }
  }
  
  def remove(value: Int): Unit = {
    if (value <= max && value >= min) {
      // value is assumed smaller than max
      val idx = indexes(value - min)
      if (idx < _size && values(idx) == value) {
        val value2 = values(_size - 1)
        swap(values, idx, _size - 1)
        swap(indexes, value - min, value2 - min)
        _size -= 1
      }
    }
  }
  
  def remove(vals: Array[Int], nbVals: Int) {
    var i = 0
    while (i < nbVals) {
      remove(vals(i))
      i += 1
    }
  }
  
  def reset(): Unit = {
    _size = 0
  }
  
  def resetTo(value: Int) {
    _size = 1
    values(0) = value
    indexes(value - min) = 0
  }
  
  def resetTo(vals: Array[Int], nbVals: Int) {
    setValues(vals, nbVals)
  }
  
  def fillArray(array: Array[Int]): Int = {
    var i = 0
    while (i < _size) {
      array(i) = values(i)
      i += 1
    }
    i
  }

  def firstValue(): Int = {
    if (_size == 0) throw new Exception("EmptySet.")
    else values(0)
  }

  private def swap(array: Array[Int], idx1: Int, idx2: Int) {
    val tmp = array(idx1)
    array(idx1) = array(idx2)
    array(idx2) = tmp
  }

  override def toString: String = Array.tabulate(_size)(i => values(i)).mkString(" ")
}

object TableSmart {
  
  def allEqual(X: Array[CPIntVar]): Constraint = {
    val tableSmart = new TableSmart(X)
    val smartTuple = new SmartTuple(tableSmart)
    for (i <- 1 until X.length)
      smartTuple.addRestrictionBinary(0, "=", i)
    tableSmart.storeTuples(Array(smartTuple))
//    smartTuples.foreach(println)
//    tableSmart
  }
  
  def notAllEqual(X: Array[CPIntVar]): Constraint = {
    val tableSmart = new TableSmart(X)
    val smartTuples = Array.ofDim[SmartTuple](X.length-1)
    for (i <- 0 until X.length-1) {
      smartTuples(i) = new SmartTuple(tableSmart)
      smartTuples(i).addRestrictionBinary(0, "!=", i+1)
    }
    tableSmart.storeTuples(smartTuples)
  } 
  
  def lexicoL(X: Array[CPIntVar], Y: Array[CPIntVar], strict: Boolean = false): Constraint = {
    assert(X.length == Y.length, "not equal sequence var length")
    val len = X.length
    val tableSmart = new TableSmart(X ++ Y)
    val smartTuples = Array.ofDim[SmartTuple](len)
    for (i <- 0 until len) {
      smartTuples(i) = new SmartTuple(tableSmart)
      for (j <- 0 until i)
        smartTuples(i).addRestrictionBinary(j, "=", j + len)
      
      val op = if (!strict && i == (len-1)) "<=" else "<"
      smartTuples(i).addRestrictionBinary(i, op, i + len)
    }
    tableSmart.storeTuples(smartTuples)
  }
  
  /**
   * tab(x) = z
   */
  def element(tab: Array[CPIntVar], x: CPIntVar, z: CPIntVar): Constraint = {
    val xLen = tab.length
    val tableSmart = new TableSmart(Array(x) ++ tab ++ Array(z))
    val smartTuples = Array.ofDim[SmartTuple](xLen)
    for (i <- 0 until xLen) {
      smartTuples(i) = new SmartTuple(tableSmart)
      smartTuples(i).addRestrictionUnary(0, "=", i).addRestrictionBinary(i+1, "=", xLen + 1)
    }
    
    tableSmart.storeTuples(smartTuples)
  }
  
  /**
   * min(X) = minimum
   */
  def minimum(X: Array[CPIntVar], minimum: CPIntVar): Constraint = {
    val xLen = X.length
    val tableSmart = new TableSmart(X ++ Array(minimum))
    val smartTuples = Array.ofDim[SmartTuple](xLen)
    for (i <- 0 until xLen) {
      smartTuples(i) = new SmartTuple(tableSmart)
      for (j <- 0 until xLen if j != i)
        smartTuples(i).addRestrictionBinary(i, "<=", j)
      smartTuples(i).addRestrictionBinary(i, "=", xLen)
    }
    tableSmart.storeTuples(smartTuples)
  }
  
  def maximum(X: Array[CPIntVar], maximum: CPIntVar): Constraint = {
    val xLen = X.length
    val tableSmart = new TableSmart(X ++ Array(maximum))
    val smartTuples = Array.ofDim[SmartTuple](xLen)
    for (i <- 0 until xLen) {
      smartTuples(i) = new SmartTuple(tableSmart)
      for (j <- 0 until xLen if j != i)
        smartTuples(i).addRestrictionBinary(i, ">=", j)
      smartTuples(i).addRestrictionBinary(i, "=", xLen)
    }
    tableSmart.storeTuples(smartTuples)
  }
  
}