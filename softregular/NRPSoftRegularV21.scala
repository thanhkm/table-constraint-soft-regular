package oscar.thesis.softregular

import java.lang.Integer.parseInt
import java.util

import oscar.cp._
import oscar.cp.constraints.Automaton

import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer
import scala.io.Source
import scala.util.Random

/**
  * NRP with soft-regular-prefix constraint.
  * In this version, we relax a percent of nurses, this is variant of version 2 (same objective as origin problem) same objective as original, but generate instance
  */
object NRPSoftRegularV21 extends App {
  val fileName = if (args.length > 0) args(0) else s"./data/nrp/Instance6.txt"
  val timeout = if (args.length > 1) args(1).toInt else 3600
  val seed = if (args.length > 2) args(2).toInt else 0
  val pRelax = if (args.length > 3) args(3).toFloat else 0.1f

  val onReq = true
  val offReq = false

  var nStaffs = 0
  var nShifts = 0
  var nDays = 0
  var nWeeks = 0
  var off = 0 // off shift, it will change to nShift after reading data

  var shifts: Array[Shift] = _
  var staffs: Array[Staff] = _

  var cover: Array[Array[Array[Int]]] = _

  val listShifts = new ArrayBuffer[Shift]()
  val listStaffs = new ArrayBuffer[Staff]()

  val mapShift = new mutable.HashMap[String, Shift]()
  val mapStaff = new mutable.HashMap[String, Staff]()

  readData(fileName)

  /*generateInstance(seed)
  println(listStaffs.mkString("\n") + "\n")*/

  val nRelax = 1//Math.round(pRelax*nStaffs) // number of relax staffs

  // Print infomation
  println(fileName)
  println("nShift: " + nShifts)
  for (i <- 0 until nShifts) {
    println(i + " shift: " + shifts(i))
  }


  // MODELING
  //-----------------------------------------------
  implicit val solver: CPSolver = CPSolver(Strong)
  val random = new Random(seed)

  // VARIABLES
  // "x[d][p] is the shift at day d for nurse p (value " + (nShifts - 1) + " denotes off)"
  //val x = Array.fill(nDays, nStaffs)(CPIntVar(0 until nShifts))
  val x = Array.tabulate(nDays, nStaffs)((d, p) => CPIntVar(0 until nShifts, "x-"+d+"-"+p))
  // "ps[p][s] is the number of days such that nurse p works with shift s"
  val ps = Array.tabulate(nStaffs, nShifts)((p, s) => if (s != off) CPIntVar(0 to staffs(p).maxs(s)) else CPIntVar(0 to nDays)) // maximum number of shifts is imposed by domains
  // "ds[d][s] is the number of nurses working on day d with shift s"
  val ds = Array.tabulate(nDays, nShifts)((d, s) => CPIntVar(0 to nStaffs)) // TODO reduce nShift demension by 1
  // "wk[p][w] is 1 iff the week-end w is worked by nurse p"
  val wk = Array.fill(nStaffs, nWeeks)(CPIntVar(0 to 1))

  // COST VARIABLES
  // cn[p][d] is the cost of not satisfying ON REQUEST for nurse p on day d
  val cn = Array.tabulate(nStaffs, nDays)((p, d) => if (staffs(p).requestFor(d, onReq) != null) CPIntVar(Array(0, staffs(p).requestFor(d, onReq).weight)) else null)
  // cf[p][d] is the cost of not satisfying OFF REQUEST for nurse p on day d
  val cf = Array.tabulate(nStaffs, nDays)((p, d) => if (staffs(p).requestFor(d, offReq) != null) CPIntVar(Array(0, staffs(p).requestFor(d, offReq).weight)) else null)
  // cc[d][s] is the cost of not satisfying cover for shift s on day d
  val cc = Array.tabulate(nDays, nShifts)((d, s) => CPIntVar(costsFor(d, s)))

  val violHorizon = Array.tabulate(nStaffs)( i => CPIntVar(0 to nDays, "violation"+i) )

  // Days off for staff
  for (d <- 0 until nDays; p <- 0 until nStaffs; if staffs(p).daysOff.contains(d)) x(d)(p).assign(off)

  // CHANNELING CONSTRAINTS
  // TODO count constraint ???
  // x to staffShift ps. nStaffs constraints
  for (p <- 0 until nStaffs) {
    add(gcc(x.map(_(p)), Array.tabulate(nShifts)(s => (s, ps(p)(s))) ))
  }
  // Maximum number of shifts
  for (p <- 0 until nStaffs) {
    add(gcc(x.map(_(p)), 0 until nShifts-1, Array.fill(nShifts-1)(0), Array.tabulate(nShifts-1)(s => staffs(p).maxs(s))))
  }

  // x to dayShift ds. nDays constraints
  for (d <- 0 until nDays) {
    add(gcc(x(d), Array.tabulate(nShifts)(s => (s, ds(d)(s))) ))
  }
  // x to worked week-ends. nStaffs * nWeeks constraints
  for (p <- 0 until nStaffs; w <- 0 until nWeeks) {
    add(((x(w * 7 + 5)(p) ?!== off) || (x(w * 7 + 6)(p) ?!== off)) === (wk(p)(w) ?=== 1))
  }

  // Maximum number of worked week-ends. nStaffs constraints
  for (p <- 0 until nStaffs) add(sum(wk(p)) <= staffs(p).maxWeek)
  // Minimum and maximum number of total worked minutes. nStaffs constraints
  for (p <- 0 until nStaffs) {
    val workedMins = weightedSum(shifts.map(_.length), ps(p))
    workedMins.updateMax(staffs(p).maxMins)
    workedMins.updateMin(staffs(p).minMins)
  }

  // Maximum consecutive worked shifts. nStaffs * (nDays - k) constraints
  for (p <- 0 until nStaffs) {
    val k = staffs(p).maxCons
    for (d <- 0 until nDays - k) add(atLeast(1, (d to d + k).map(i => x(i)(p)), off)) // at least 1 off shift for k + 1 days
  }

  //--- REGULAR CONSTRAINT -------
  val relaxedStaffs = getUnfixedStaffs(nRelax)
  println("Relaxed size: " + relaxedStaffs.size)

  // Rotation constraint
  val automaton = automataForRotation()
  for (p <- 0 until nStaffs) {
    if (!relaxedStaffs.contains(p)) add(regular(x.map(_ (p)), automaton))
    else add(new SoftRegularPrefixMDD(x.map(_(p)), automaton, violHorizon(p)))
  }

  // Minimum consecutive worked shifts
  val maxConsShift = staffs.map(_.minCons).max
  var vars = Array.fill(maxConsShift)(CPIntVar(0 until off)) // redundant variables
  for (p <- 0 until nStaffs) {
    val k = staffs(p).minCons
    if (k > 1) {
      if (!relaxedStaffs.contains(p)) add(regular(vars ++ x.map(_(p)) ++ vars, automataForMinConsecutiveShifts2(k)))
      else add(new SoftRegularPrefixMDD(vars ++ x.map(_(p)) ++ vars, automataForMinConsecutiveShifts2(k), violHorizon(p)))
    }
  }

  // Minimum consecutive days off
  val maxConsOff = staffs.map(_.minOff).max
  vars = Array.fill(maxConsOff)(CPIntVar(off)) // redundant variables
  for (p <- 0 until nStaffs) {
    val k = staffs(p).minOff
    if (k > 1) {
      if (!relaxedStaffs.contains(p)) add(regular(vars ++ x.map(_(p)) ++ vars, automataForMinConsecutiveOff2(k)))
      else add(new SoftRegularPrefixMDD(vars ++ x.map(_(p)) ++ vars, automataForMinConsecutiveOff2(k), violHorizon(p)))
    }
  }

  // half of horizon must be satisfied
  for (p <- 0 until nStaffs)
    if (relaxedStaffs.contains(p)) add(violHorizon(p) <= nDays/2) else add(violHorizon(p) === 0)

  //------ COST CONSTRAINTS ------------
  // Cost for not satisfying on requests
  for (p <- 0 until nStaffs; d <- 0 until nDays) {
    val req = staffs(p).requestFor(d, onReq)
    if (req != null) add((x(d)(p) ?=== req.shift.num) === (cn(p)(d) ?=== 0))
  }
  // Cost for not satisfying off requests
  for (p <- 0 until nStaffs; d <- 0 until nDays) {
    val req = staffs(p).requestFor(d, offReq)
    if (req != null) add((x(d)(p) ?=== req.shift.num) === (cf(p)(d) ?!== 0))
  }
  // Cost of under or over covering. nDays * nShifts constraints
  for (d <- 0 until nDays; s <- 0 until nShifts) {
    val tuples = costsFor(d, s).zipWithIndex.map { case (c, i) => Array(i, c)}
    add(table(Array(ds(d)(s), cc(d)(s)), tuples))
  }

  //-- PROCCESSING WHEN SOLUTION FOUND -------
  // Variables using for display results
  val psT = ps.transpose
  val wkT = wk.transpose
  val cnT = cn.transpose
  val cfT = cf.transpose

  // Variables using for LNS
  val bestSol = Array.fill(nDays, nStaffs)(off) // best solution found for x[d][p]
  val bestCC = Array.fill(nDays, nShifts)(0) // best solution found for covering
  var bestViol = Array.fill(nStaffs)(Int.MaxValue)
  var bestCost = Int.MaxValue

  val dayCost = Array.fill(nDays)(0) // sum of penalty for each day in the horizon

  //------- PRINT RESULTS AND UPDATE SOLUTION --------
  onSolution {
    for (d <- 0 until nDays; p <- 0 until nStaffs) bestSol(d)(p) = x(d)(p).value
    for (p <- 0 until nStaffs) bestViol(p) = violHorizon(p).value
    bestCost = cost.value

    for (d <- 0 until nDays; p <- 0 until nShifts)
      bestCC(d)(p) = if (cc(d)(p).value < 100) cc(d)(p).value*101 else cc(d)(p).value
    for (d <- 0 until nDays)
      dayCost(d) = bestCC(d).sum + cnT(d).filter(_ != null).map(_.value).sum + cfT(d).filter(_ != null).map(_.value).sum

    // Variables
    println("x(p)(d)")
    val display = Array.fill(nDays, nStaffs)("")
    for (d <- 0 until nDays; p <- 0 until nStaffs)
      display(d)(p) = if (staffs(p).daysOff.contains(d)) "x" else if (x(d)(p).value == off) "-" else x(d)(p).value+"" //if (staffs(p).daysOff.contains(d)) "x" else
    display.transpose.foreach(d => println(d.mkString(" ")))

    println("cc(d)(s)")
    cc.foreach(xx => println(xx.map(_.value).dropRight(1).mkString("\t")))

    println("Cost: " + cost)
    println("Viol: " + bestViol.sum + " | " + bestViol.mkString(" "))
    println("---\n")
  }

  //---------- OBJECTIVE -----------
  //val cost = sum(cc.flatten)
  val cost = sum(Array(cc.flatten, cn.flatten.filter(_ != null), cf.flatten.filter(_ != null)).flatten)
  minimize(cost)

  val xx = x.transpose.flatten ++ violHorizon

  //------------ GET INIT SOLUTION ---------
  // TODO: Replace by an greedy solution
  search {binaryStatic(xx)} // random and min can affect the final value
  println("Searching for initial solution")
  val stats = start(nSols = 1, timeLimit = 600)
  println(stats)


  //------------------- LNS -----------------
  //-----------------------------------------
  //------------ SEARCH FOR LNS -------------
  val mapVarDay = mutable.HashMap[CPIntVar, Int]() // (violHorizon, -1)
  for (d <- 0 until nDays; p <- 0 until nStaffs) mapVarDay += (x(d)(p) -> d)
  val mapVarStaff = mutable.HashMap[CPIntVar, Int]() // (violHorizon, -1)
  for (d <- 0 until nDays; p <- 0 until nStaffs) mapVarStaff += (x(d)(p) -> p)
  for (p <- 0 until nStaffs) {
    mapVarDay += (violHorizon(p)-> -1)
    mapVarStaff += (violHorizon(p) -> -1)
  }

  // Compute ranking for selecting variable in search
  // this ranking takes account of cost by day for each variable
  def getRankingFor(varId: Int): Int = {
    val d = mapVarDay(xx(varId))
    if (d == -1) d
    else dayCost(mapVarDay(xx(varId)))
  }

  // Finding the shift with highest cost
  // TODO using cost cover cc to compute more dynamically
  def getValueFor(varId: Int): Int = {
    val d = mapVarDay(xx(varId))
    if (d == -1) xx(varId).min
    else {
      val dom = xx(varId).iterator.toArray.sortBy(s => if (bestCC(d)(s) % 100 == 0) -bestCC(d)(s) else bestCC(d)(s))
      if (random.nextInt(100) < 20) xx(varId).randomValue(random)
      else dom.head
    }
  }

  // Using weighted of cc(d)(s) variable for search, change it to reduce the cost in a fastest way
  search {
    //conflictOrderingSearch(xx, xx(_).size, xx(_).min)
    //binaryLastConflict(xx, xx(_).size, xx(_).randomValue(random))
    binaryLastConflict(xx, i => xx(i).size - getRankingFor(i), i => getValueFor(i)) // i is index of variable
  }


  // TODO IDEAS: Computing Lower Bound of Overall Cost during Search, decompose problem in
  // many steps with relaxation, equalize shift assignment for each staff
  //----------- VNS/LDS+CP --------------
  println("\n\nVNS/LDS+CP")
  val kInit = 1 + 0.0001
  val startTime = System.currentTimeMillis()

  // a window using by LNS
  def getWindow(window: Int): (Int, Int) = {
    val taken = random.nextInt(dayCost.sum)
    var i = 0
    var sum = dayCost(i)
    while (sum < taken) {
      i += 1
      sum += dayCost(i)
    }

    // TODO: which window is better? first or second?
    var start = math.min(i, nDays-i); var end = start + window
    //var start = Math.max(0, i - window/2 - 1); var end = Math.min(nDays, i + window/2 + 1)

    if (start < 5) start = 0; if (end > nDays-5) end = nDays
    (start, end)
  }

  var runtime = 0
  var k = kInit // k number of unfixed staffs schedule
  while (runtime < 3*timeout/4) {
    // Strategy 3: combine 2 previous, take some random staff and select a window for relaxation
    // Number of unfixed staff is equal to k
    val unfixedStaffs = getUnfixedStaffs(k.toInt)
    // window is about 25% to 50% of the horizon
    val window = Math.min(Math.max(nDays * (25 + random.nextInt(50)) / 100, 14), 35) // window is between 14 and 35 day
    //val (start, end) = getWindow(math.min(window, 20))

    println("uStaff="+ unfixedStaffs.size)
    // TODO try stats3 many time before stats4
    var stats3 = startSubjectTo(timeLimit = 0) {}
    var i = 0
    while (stats3.nSols == 0 && i < nDays) {
      val (start, end) = (i, Math.min(nDays, i + window))
      println(" start=" + start + " end=" + end + " runtime=" + ((System.currentTimeMillis() - startTime)/1000) + "s")

      stats3 = startSubjectTo(failureLimit = 50000 + k.toInt*10000){
        // fixed other staffs
        (0 until nStaffs).filter(!unfixedStaffs.contains(_)).foreach(p => add((0 until nDays).map(d => x(d)(p) === bestSol(d)(p))))
        // fixed schedule which is outside of the window
        (0 until nDays).filter(d => d < start || d >= end).foreach(d => add((0 until nStaffs).map(p => x(d)(p) === bestSol(d)(p))))
      }

      i = if (end >= nDays) nDays else i + window/3 // move window /3 or /2
    }
    var stats4 = stats3
    if (stats3.nSols == 0 && random.nextInt(100) < 70) {
      println(" start=" + 0 + " end=" + nDays + " runtime=" + ((System.currentTimeMillis() - startTime)/1000) + "s")

      stats4 = startSubjectTo(failureLimit = 100000 + k.toInt*20000){
        (0 until nStaffs).filter(!unfixedStaffs.contains(_)).foreach(p => add((0 until nDays).map(d => x(d)(p) === bestSol(d)(p))))
      }
    }

    // Move or not: change neighborhood or not
    k = if (stats3.nSols == 0 && stats4.nSols == 0) k + 0.1 else kInit

    runtime = ((System.currentTimeMillis() - startTime)/1000).toInt
  }

  // --------------------------------------------------
  // --------------------------------------------------
  // ----- Last try to minimize violated horizon ------
  // --------------------------------------------------
  // --------------------------------------------------
  println("last try")
  minimize(sum(violHorizon)) //cost +
  k = 1
  while (runtime < timeout) {
    // Number of unfixed staff is equal to k
    val unfixedStaffs = getUnfixedStaffs(k.toInt)++relaxedStaffs
    // window is about 25% to 50% of the horizon
    val window = Math.min(Math.max(nDays * (25 + random.nextInt(50)) / 100, 14), 35)

    println("uStaff="+ unfixedStaffs.size)
    var stats3 = startSubjectTo(timeLimit = 0) {}
    var i = 0
    while (stats3.nSols == 0 && i < nDays) {
      val (start, end) = (i, Math.min(nDays, i + window))
      stats3 = startSubjectTo(failureLimit = 50000 + k.toInt*10000) {
        // fixed other staffs and fixed schedule which is outside of the window
        (0 until nStaffs).filter(!unfixedStaffs.contains(_)).foreach(p => add((0 until nDays).map(d => x(d)(p) === bestSol(d)(p))))
        (0 until nDays).filter(d => d < start || d >= end).foreach(d => add((0 until nStaffs).map(p => x(d)(p) === bestSol(d)(p))))
      }
      i = if (end >= nDays) nDays else i + window/3 // move window
    }
    var stats4 = stats3
    if (stats3.nSols == 0 && random.nextInt(100) < 70) {
      stats4 = startSubjectTo(failureLimit = 100000 + k.toInt*20000) {
        (0 until nStaffs).filter(!unfixedStaffs.contains(_)).foreach(p => add((0 until nDays).map(d => x(d)(p) === bestSol(d)(p))))
      }
    }
    // Move or not: change neighborhood or not
    k = if (stats3.nSols == 0 && stats4.nSols == 0) k + 0.1 else kInit

    runtime = ((System.currentTimeMillis() - startTime)/1000).toInt
  }

  println("Time=" + ((System.currentTimeMillis() - startTime)/1000) + "s")
  println("Best cost found: ")
  println(bestCost + " & " + bestViol.sum + " & " + bestViol.mkString(" "))

  // TODO make choice of staff by some weight
  // Purely random variable selection
  def getUnfixedStaffs(nbUnfixedStaffs: Int) = {
    val unfixedStaffs = collection.mutable.Set[Int]()
    val reviseStaffs = collection.mutable.Set[Int]() ++ (0 until nStaffs).toSet // create mutable set

    while (unfixedStaffs.size < nbUnfixedStaffs && reviseStaffs.nonEmpty) {
      val staff = reviseStaffs.toVector(random.nextInt(reviseStaffs.size))
      reviseStaffs -= staff; unfixedStaffs += staff
    }
    unfixedStaffs
  }


  //println(start(timeLimit = 3000, maxDiscrepancy = 10))
  //--------------------------------------------------------
  //--------------------------------------------------------
  /**
    * Create automaton for min k consecutive shifts
    */
  def automataForMinConsecutiveShifts2(k: Int): Automaton = {
    val nStates = k + 1
    val nLetters = nShifts
    val initState = 0
    val finiteStates = new java.util.HashSet[Integer]()
    finiteStates.add(0)
    finiteStates.add(k)
    val automaton = new Automaton(nStates, nLetters, initState, finiteStates)

    automaton.addTransition(0, 0, off)
    for (state <- 0 until k; letter <- 0 until off) automaton.addTransition(state, state+1, letter)
    for (letter <- 0 until off) automaton.addTransition(k, k, letter)
    automaton.addTransition(k, 0, off)

    automaton
  }

  def automataForMinConsecutiveOff2(k: Int): Automaton = {
    val nStates = k + 1
    val nLetters = nShifts
    val initState = 0
    val finiteStates = new java.util.HashSet[Integer]()
    finiteStates.add(0)
    finiteStates.add(k)
    val automaton = new Automaton(nStates, nLetters, initState, finiteStates)

    for (letter <- 0 until off) automaton.addTransition(0, 0, letter)
    for (state <- 0 until k) automaton.addTransition(state, state+1, off)
    automaton.addTransition(k, k, off)
    for (letter <- 0 until off) automaton.addTransition(k, 0, letter)

    automaton
  }

  def automataForRotation(): Automaton = {
    val nStates = nShifts
    val nLetters = nShifts
    val initState = 0
    val finiteStates = new util.HashSet[Integer]()
    for (s <- 0 until nStates) finiteStates.add(s)
    val automaton = new Automaton(nStates, nLetters, initState, finiteStates)

    for (s <- 0 until nStates) {
      val shift = shifts(s)
      val notFollowings = shift.notFollowing.map(_.num)
      for (l <- 0 until nLetters) {
        if (!notFollowings.contains(l))
          automaton.addTransition(s, l, l)
      }
    }

    automaton
  }

  def costsFor(d: Int, s: Int): Array[Int] = {
    if (s == off) Array.fill(nStaffs + 1)(0)
    else Array.tabulate(nStaffs + 1)(i =>
      if (i <= cover(d)(s)(0)) (cover(d)(s)(0) - i) * cover(d)(s)(1) else (i - cover(d)(s)(0)) * cover(d)(s)(2))
  }

  def coverFor(d: Int, s: Int): Int = {
    if (s == off) 0 else cover(d)(s)(0)
  }

  def getRotation: Array[Array[Int]] =
    shifts.filter(_.notFollowing.nonEmpty).flatMap(s => s.notFollowing.map(sh => Array(s.num, sh.num)))

  def readData(fileName: String): Unit = {
    val lines = Source.fromFile(fileName).getLines()

    def skipComment(): String = {
      var line = lines.next().trim
      while (line.length == 0 || line(0) == '#') {
        line = lines.next()
      }
      line.trim
    }

    def process(headString: String, function: Array[String] => Unit): Unit = {
      var line = skipComment()
      assert(line == headString)
      line = skipComment()
      while (line != null && line.length > 0) {
        // apply function
        function(line.split(","))
        line = if (lines.hasNext) lines.next().trim else null
      }
    }

    val line = skipComment()
    assert(line == "SECTION_HORIZON")
    nDays = parseInt(skipComment())
    nWeeks = nDays / 7

    process("SECTION_SHIFTS", tokens => new Shift(tokens))
    listShifts += new Shift() // add off shift
    listShifts.foreach(_.processNotFollowing())
    shifts = listShifts.toArray

    process("SECTION_STAFF", tokens => new Staff(tokens))
    staffs = listStaffs.toArray

    process("SECTION_DAYS_OFF", tokens => mapStaff(tokens(0)).daysOff = tokens.tail.map(parseInt))

    process("SECTION_SHIFT_ON_REQUESTS", tokens => mapStaff(tokens(0)).
      onRequests += new Request(parseInt(tokens(1)), mapShift(tokens(2)), parseInt(tokens(3))))

    process("SECTION_SHIFT_OFF_REQUESTS", tokens => mapStaff(tokens(0)).
      offRequests += new Request(parseInt(tokens(1)), mapShift(tokens(2)), parseInt(tokens(3))))

    cover = Array.fill(nDays, nShifts - 1, 3)(0)
    process("SECTION_COVER", tokens =>
      cover(parseInt(tokens(0)))(mapShift(tokens(1)).num) = tokens.tail.tail.map(parseInt))

    // print infos
    println("nDays=" + nDays + " nWeeks=" + nWeeks + " nShifts=" + nShifts + " nStaffs=" + nStaffs + " off=" + off)
//    println(listShifts.mkString("\n") + "\n")
//    println(listStaffs.mkString("\n") + "\n")
//    println(cover.flatten.map(_.mkString(",")).mkString("\n") + "\n")

  }

  def generateInstance(seed: Int): Unit = {
    val rand = new Random(seed)

    for (p <- 0 until nStaffs) {
      val staff = staffs(p)
      // make random day offs for each staff
      staff.daysOff = getSet(staff.daysOff.length).toArray.sorted
      // make random on requests
      val onSeq = getSet(staff.onRequests.length).toArray.sorted
      for (i <- onSeq.indices) staff.onRequests(i).day = onSeq(i)
      // make random off requests
      val offSeq = getSet(staff.offRequests.length).toArray.sorted
      for (i <- offSeq.indices) staff.offRequests(i).day = offSeq(i)
    }

    // get a random set with size from 0..nDays-1
    def getSet(size: Int) = {
      val choose = collection.mutable.Set[Int]()
      val toChoose = collection.mutable.Set[Int]() ++ (0 until nDays).toSet // create mutable set

      while (choose.size < size) {
        val element = toChoose.toVector(rand.nextInt(toChoose.size))
        choose += element; toChoose -= element
      }
      choose
    }
  }

  class Shift(val num: Int, val id: String, val length: Int, val notFollowingStr: String = null) {
    val notFollowing = ArrayBuffer[Shift]()

    // constructor for Off
    def this() = {
      this(nShifts, "off", 0)
      off = nShifts
      nShifts += 1
    }

    def this(tokens: Array[String]) = {
      this(nShifts, tokens(0), parseInt(tokens(1)), if (tokens.length == 2) null else tokens(2))
      listShifts += this
      mapShift += (this.id -> this)
      nShifts += 1
    }

    def processNotFollowing(): Unit = {
      if (notFollowingStr != null) {
        notFollowingStr.split("\\|").foreach(s => notFollowing += listShifts.filter(_.id == s).head)
      }
    }

    override def toString: String = num + " " + id + " " + length + " " + notFollowing.map(sh => sh.id).mkString(",")
  }

  class Request(var day: Int, val shift: Shift, val weight: Int) {
    override def toString: String = day + ":" + shift.id + ":" + weight
  }

  class Staff(val num: Int, val id: String, val maxs: Array[Int],
              val maxMins: Int, val minMins: Int, val maxCons: Int, val minCons: Int, val minOff: Int, val maxWeek: Int) {

    var daysOff = Array[Int]()
    val onRequests = ArrayBuffer[Request]()
    val offRequests = ArrayBuffer[Request]()

    def requestFor(day: Int, on: Boolean): Request = {
      val req = (if (on) onRequests else offRequests).filter(r => r.day == day)
      if (req.isEmpty) null else req.head
    }

    def this(tokens: Array[String]) = {
      this(nStaffs, tokens(0), tokens(1).split("\\|").map(s => parseInt(s.substring(s.indexOf("=") + 1))),
        parseInt(tokens(2)), parseInt(tokens(3)), parseInt(tokens(4)), parseInt(tokens(5)), parseInt(tokens(6)), parseInt(tokens(7)))

      listStaffs += this
      mapStaff += (this.id -> this)
      nStaffs += 1
    }

    override def toString: String = num + " " + id + " " + maxs.mkString(",") + " (" + maxMins + "," + minMins + ") (" + maxCons + "," + minCons + ") " +
      minOff + " " + maxWeek + "\n  dayOff=" + daysOff.mkString(",") + "\n  onRequ=" + onRequests.mkString(",") + "\n  offRequ=" + offRequests.mkString(",")
  }

}

