package oscar.thesis.softregular

import oscar.cp.constraints.Automaton
import oscar.cp.core._
import oscar.cp.core.delta.DeltaIntVar
import oscar.cp.core.variables.{CPIntVar, CPVar}

import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer

/**
  * Soft-regular prefix constraint based on MDD propagation
  *     Soft-regular with a Prefix-size Violation Measure. CPAIOR 2015 Delft.
  *
  * @author ThanhKM
  */
class SoftRegularPrefixMDD(X: Array[CPIntVar], automaton: Automaton, z: CPIntVar) extends Constraint(X(0).store, "SoftRegularPrefixMDD") {

  override def associatedVars(): Iterable[CPVar] = X++Array(z)

  // number of variables
  private[this] val arity = X.length
  private[this] val deltaVars = new Array[DeltaIntVar](arity) /* values delete since last propagation */
  private[this] val maxDomSize = X.map(_.size).max /* max domain size of all variables */
  private[this] val tmpValues = new Array[Int](maxDomSize) /* Use to store values during propagation */

  //-------- Information of DFA --------
  // automaton.setPosted()   // fix automaton
  private[this] val nbLetters = automaton.getNbLetters
  private[this] val nbStates = automaton.getNbStates
  private[this] val nullState = automaton.getNullState
  private[this] val initState = automaton.getInitialState
  private[this] val finiteStates = automaton.getAcceptingStates
  private[this] val T = automaton.getTransitionMatrix; // T[state1][letter] = state2

  // all nodes in the graph
  val nbLayers = arity + 1
  private[this] val layerNodes = Array.tabulate(nbLayers)(l => Array.tabulate(nbStates)(s => new Node(s, l)))

  private[this] val root: Node = layerNodes(0)(initState)
  private[this] var timeStamp = 0L

  //------------------------------------
  override def setup(l: CPPropagStrength): Unit = {
    if (z.max > arity)
      throw new Exception("This constraint is not helpful, it permits to violate more than number of its variables")

    // build graph for this constraint
    buildGraph()

    propagate()

    var i = 0
    while (i < arity) {
      deltaVars(i) = X(i).callPropagateOnChangesWithDelta(this)
      i += 1
    }
    z.callPropagateWhenDomainChanges(this)
  }

  private val exploredNodes = new mutable.HashMap[Node, Int]()
  private val gacValues = Array.tabulate(arity)(i => Array.fill(X(i).max+1)(0L))
  //private val nGACValues = Array.fill(arity)(0)

  override def propagate(): Unit = {
    // reset gacValue is done by increasing timeStamp by 1
    timeStamp += 1
    exploredNodes.clear()

    val maxLevel = explore(root)
    val minBound = math.max(z.min, arity - maxLevel)

    // update lower bound of cost variable
    // if not throw inconsistency, update other variable domain
    z.updateMin(minBound)

    /* Update variables' domain */
    var varID = arity - z.max
    while (varID > 0) {
      varID -= 1

      val nbValues = X(varID).fillArray(tmpValues)
      var i = nbValues
      while (i > 0) {
        i -= 1
        val value = tmpValues(i)
        if (gacValues(varID)(value) != timeStamp) {
          X(varID).removeValue(value)
        }
      }
    }

  }

  /**
    * Explore the associated layered graph, and return the deepest level can reach
    */
  private def explore(node: Node): Int = {
    if (node.level == arity)
      return arity

    if (exploredNodes.contains(node))
      return exploredNodes.apply(node)

    val varID = node.varID
    /* Should be collected GAC for this node or not */
    val threshold = arity - z.max
    val mustCollect = node.level < threshold

    var maxLevel = node.level
    for (arc <- node.outArcs) {
      if (X(varID).hasValue(arc.value)) {
        val depth = explore(arc.dest)
        if (mustCollect && depth >= threshold) {
          // add this value to GAC set by updating timeStamp
          gacValues(varID)(arc.value) = timeStamp
        }
        maxLevel = math.max(maxLevel, depth)
      }
    }

    exploredNodes.put(node, maxLevel)

    maxLevel
  }

  /**
    * Build the associated layered graph with the constraint regular
    */
  private def buildGraph(): Unit = {

    /* Forward phrase: add all arcs in graph */
    val visited = Array.fill(arity + 1)(Array.fill(nbStates)(false))
    val queue = new mutable.Queue[Node]()
    queue.enqueue(root)
    while (queue.nonEmpty) {
      val node = queue.dequeue()
      if (node.level < arity && !visited(node.level)(node.state)) {
        visited(node.level)(node.state) = true // set visited

        val varID = node.varID
        val nbValues = X(varID).fillArray(tmpValues)
        var i = nbValues
        while (i > 0) {
          i -= 1
          val value = tmpValues(i)
          if (value < nbLetters && value >= 0 && T(node.state)(value) != nullState) {
            val nextNode = layerNodes(node.level+1)(T(node.state)(value))
            val arc = new Arc(node, value, nextNode)
            node.addOutArc(arc)
            nextNode.addInArc(arc)
            // put this node in the queue
            queue.enqueue(nextNode)
          }
        }
      }
    }

    /* Backward phrase: remove arcs that not lead to finite states */
    queue.clear()
    var s = nbStates
    while (s > 0) {
      s -= 1
      if (!finiteStates.contains(s)) {
        val node = layerNodes(arity)(s) // node in last layer
        if (node.inArcs.nonEmpty) {
          queue.enqueue(node)
        }
      }
    }
    // This procedure does not need to verify if a node is
    // visited or not, since it appears in queue at most one.
    while (queue.nonEmpty) {
      val node = queue.dequeue()
      for (arc <- node.inArcs) {
        val prevNode = arc.source
        if (prevNode.removeOutArc(arc) == 0) {
          queue.enqueue(prevNode)
        }
      }
    }

  }


  class Node(val state: Int, val level: Int) {
    def varID = level //if (level < arity) level else throw new Exception("no variable for last layer")// = varID? except last level
    val inArcs = ArrayBuffer[Arc]()
    val outArcs = ArrayBuffer[Arc]()

    def addInArc(arc: Arc) = inArcs += arc
    def addOutArc(arc: Arc) = outArcs += arc
    def removeInArc(arc: Arc) = {
      inArcs -= arc
      inArcs.size
    }
    def removeOutArc(arc: Arc) = {
      outArcs -= arc
      outArcs.size
    }

  }

  class Arc(val source: Node, val value: Int, val dest: Node) {
  }
}

