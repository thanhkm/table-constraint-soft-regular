package oscar.thesis.softregular

import oscar.cp._
import oscar.cp.constraints.Automaton
import oscar.cp.constraints.tables.{TableSTR2Reif, TableSTRNe}
import oscar.cp.core._
import oscar.cp.core.variables.{CPBoolVar, CPIntVar, CPVar}

/**
  * Decomposition class for soft-regular-prefix
  *
  * @author ThanhKM thanhkhongminh@gmail.com
  */
class SoftRegularPrefixDec(x: Array[CPIntVar], automaton: Automaton, z: CPIntVar) extends Constraint(x(0).store, "SoftRegularPrefixDec") {

  override def associatedVars(): Iterable[CPVar] = x++Array(z)

  val arity = x.length

  //automaton.setPosted() //ensure that the automaton is not modified anymore
  val nbStates = automaton.getNbStates
  val nullState = automaton.getNullState
  val T = automaton.getTransitionMatrix
  // tuples for (state1, letter, state2)
  val tuples = (for (s <- T.indices; l <- T(s).indices; if T(s)(l) != nullState) yield Array(s, l, T(s)(l))).toArray
  val negTuples = tuples.flatMap(tau => for (s <- 0 to nbStates; if tau(2) != s) yield Array(tau(0), tau(1), s))

  val initState = automaton.getInitialState
  val acceptingStates = automaton.getAcceptingStates

  val y = Array.fill(arity+1)(CPIntVar(0 until nbStates)(s)) // the value nbStates represent bottom
  y(0).assign(initState) // D(y0) = {initState}
  (0 until nbStates).foreach(v => if (!acceptingStates.contains(v)) y(arity).removeValue(v)) // D(yn) = acceptingStates

  override def setup(l: CPPropagStrength): Unit = {
    if (z.max > arity)
      throw new Exception("This constraint is not helpful, it permits to violate more than number of its variables")

    // reify constraints
    val b = Array.fill(arity)(CPBoolVar()(s))
    for (i <- 0 until arity) {
      s.post(new TableSTR2Reif(Array(y(i), x(i), y(i + 1)), tuples, b(i)))
    }

    // functional constraints
    for (i <- 0 until arity-1) {
      s.post(new TableSTRNe(Array(y(i), x(i), y(i + 1)), negTuples))
    }

    for (i <- 0 until arity) {
      s.post((b(i) ?=== 0) ==> (z ?>= (arity - i)))
    }

    for (i <- 1 until arity) {
      val cond = sum(b.take(i))
      s.post((z ?<= (arity-i)) ==> (cond ?=== i))
    }

  }

}
