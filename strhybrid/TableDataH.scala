package oscar.thesis.strhybrid

/**
  * Table data for algorithm STR-Hybrid
  *
  */
class TableDataH(table: Array[Array[Int]]) {

  private[this] val arity = table(0).length
  // size of table is supposed bigger than 0
  private[this] val tableSize = table.length // size of table

  private[this] val subTableSize = Array.fill(arity)(Array[Int]())
  private[this] val firstEqual = Array.fill(arity)(Array[Int]())
  private[this] val nextEqual = Array.fill(arity)(Array[Int]())
  private[this] val nextDifferent = Array.fill(arity)(Array[Int]())

  // max, min value of variables' domain
  val max = Array.fill(arity)(Int.MinValue)
  val min = Array.fill(arity)(Int.MaxValue)

  def setup(): Unit = {
    initMinMax()
    initDataStructure()

    // Initialization firstEqual, nextEqual, nextDifferent 
    var i = tableSize - 1
    while (i >= 0) {
      val tau = table(i)
      var varId = 0
      while (varId < arity) {
        val valuePosition = tau(varId) - min(varId)
        nextEqual(varId)(i) = firstEqual(varId)(valuePosition)
        firstEqual(varId)(valuePosition) = i
        subTableSize(varId)(valuePosition) += 1
        if (i < tableSize - 1) {
          if (tau(varId) != table(i + 1)(varId)) {
            nextDifferent(varId)(i) = i + 1
          } else {
            nextDifferent(varId)(i) = nextDifferent(varId)(i + 1)
          }
        }
        varId += 1
      }

      i -= 1
    }

  }

  @inline private def initDataStructure(): Unit = {
    var varId = 0
    while (varId < arity) {
      val nValues = max(varId) - min(varId) + 1
      subTableSize(varId) = Array.fill(nValues)(0)
      firstEqual(varId) = Array.fill(nValues)(-1)
      nextEqual(varId) = Array.fill(tableSize)(-1)
      nextDifferent(varId) = Array.fill(tableSize)(-1)

      varId += 1
    }
  }

  @inline private def initMinMax(): Unit = {
    var i = 0
    while (i < tableSize) {
      val tau = table(i)
      var varId = 0
      while (varId < arity) {
        min(varId) = min(min(varId), tau(varId))
        max(varId) = max(max(varId), tau(varId))
        varId += 1
      }
      i += 1
    }
  }

  @inline def subTableSize(varId: Int, value: Int): Int = {
//    if (value < min(varId) || value > max(varId)) 0
//    else subTableSize(varId)(value - min(varId))
    subTableSize(varId)(value - min(varId))
  }

  @inline def firstEq(varId: Int, value: Int): Int = {
    firstEqual(varId)(value - min(varId))
  }

  @inline def nextEq(varId: Int, tid: Int): Int = {
    nextEqual(varId)(tid)
  }

  @inline def nextDiff(varId: Int, tid: Int): Int = {
    nextDifferent(varId)(tid)
  }

  @inline def min(a: Int, b: Int): Int = {
    if (a < b) a
    else b
  }

  @inline def max(a: Int, b: Int): Int = {
    if (a > b) a
    else b
  }
}