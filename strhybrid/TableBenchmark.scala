package oscar.thesis.strhybrid

import oscar.cp._

import scala.sys.process._
import scala.xml.XML

/**
 *  Table benchmark for XCSP2 instance. Modify from code of Pierre Schaus.
 */
object TableBenchmark extends App {
  val TIME_OUT = if (args.length > 2) args(2).toInt else 1200

  var instance = if (args.length > 0) args(0)
      else  "/Users/ThanhKM/workspace/xcsp-bench/bench/bddLarge/bdd-21-2713-15-79-1_ext.xml"
  val implem = if (args.length > 1) args(1) else "STRHybrid"

  // for bz2 instance
  var toRemove = false
  if (instance.contains(".bz2")) {
    toRemove = true
    "bzip2 -k -d " + instance !

    instance = instance.substring(0, instance.length-4)
  }

  println(implem)
  println(instance.split("\\/").last)


  import oscar.cp.constraints.tables.TableAlgo._
  import oscar.cp.constraints.tables._

  val implems = Map(
    //"SC" -> ((x: Array[CPIntVar], tuples: Array[Array[Int]]) => new TableSC(x, tuples)),
    "CT" -> ((x: Array[CPIntVar], tuples: Array[Array[Int]]) => new TableCT(x, tuples)),
    "CTStar" -> ((x: Array[CPIntVar], tuples: Array[Array[Int]]) => table(x, tuples, CompactTableStar)),
    "STR2" -> ((x: Array[CPIntVar], tuples: Array[Array[Int]]) => table(x, tuples,STR2)),
    "STRHybrid" -> ((x: Array[CPIntVar], tuples: Array[Array[Int]]) => new TableSTRHybrid(x, tuples)),
    "STR3" -> ((x: Array[CPIntVar], tuples: Array[Array[Int]]) => table(x, tuples,STR3)),
    "GAC4" -> ((x: Array[CPIntVar], tuples: Array[Array[Int]]) => table(x, tuples,GAC4)),
    "GAC4R" -> ((x: Array[CPIntVar], tuples: Array[Array[Int]]) => table(x, tuples,GAC4R)),
    "MDD4R" -> ((x: Array[CPIntVar], tuples: Array[Array[Int]]) => table(x, tuples,MDD4R)),
    "AC5TCR" -> ((x: Array[CPIntVar], tuples: Array[Array[Int]]) => table(x, tuples,AC5TCRecomp))
  )

  implicit val solver: CPSolver = CPSolver()
  val xml = XML.loadFile(instance)

  // remove the file
  if (toRemove) {
    "rm -f " + instance !
  }


  /* Domains of the variables */
  val domains = (xml \ "domains" \ "domain").map { line =>
    val name = (line \ "@name").text
    val nbValues = (line \ "@nbValues").text.toInt

    val values = if (line.text.contains("..")) {
      val rangeBounds = line.text.split("\\.\\.").map(_.trim.toInt)
      val range = rangeBounds(0) to rangeBounds(rangeBounds.length - 1)
      range.toSet
    } else {
      line.text.trim.split(" ").map(_.toInt).toSet
    }

    assert(values.size == nbValues)
    (name, values)
  }.toMap

  /* Variables */
  val varsArray = (xml \ "variables" \ "variable").map { line =>
    val name = (line \ "@name").text
    val domain = (line \ "@domain").text

    (name, CPIntVar.sparse(domains(domain), name))
  }
  val indexOfVar = varsArray.map(_._2).zipWithIndex.toMap
  val vars = varsArray.toMap


  /* Tuples */
  val relations = (xml \ "relations" \ "relation").map { line =>
    val name = (line \ "@name").text
    val nbTuples = (line \ "@nbTuples").text.toInt
    //println(line)
    val tuples = line.text.trim.split("\\|").map(tup => tup.trim.split(" ").map(_.trim.toInt))
    assert(nbTuples == tuples.length)
    (name, tuples)
  }.toMap


  var beforeInit = 0L
  var endInit = 0L
  var beforeSearch = 0L
  var finalTime = 0L
  try {
    /* Constraints */
    val constraints = (xml \ "constraints" \ "constraint").map { line =>
      val name = (line \ "@name").text
      val scopeName = (line \ "@scope").text.split(" ")
      val scope = scopeName.map(vars(_))
      val tab = relations((line \ "@reference").text)
      (name, scope, tab)
    }

    /* We add the constraints */
    beforeInit = System.currentTimeMillis()
    val theConstraints = constraints.map(p => implems(implem)(p._2,p._3))
    add(theConstraints.reverse)

    endInit = System.currentTimeMillis()
    println("end init")

    /* Search for a solution */
    val X = varsArray.map(_._2).toArray
    var degree = Array.fill(X.length)(0)
    val varIndex = X.zipWithIndex.toMap
    constraints.foreach { c =>
      for (x <- c._2) {
        degree(varIndex(x)) += 1
      }
    }
    //println(degree.mkString(","))
    val Y = X.sortBy(x => - degree(varIndex(x)))

    search {
      //binaryStatic(Y)
      //binaryFirstFail(Y,_.min)
      binaryIdx(X, i => -(degree(i)<<7) / X(i).size,i => X(i).min)
      //conflictOrderingSearch(X, i => X(i).size,i => X(i).min)
    }

    beforeSearch = System.currentTimeMillis()

    println("start search")
    //println(Runtime.getRuntime().totalMemory())
    val stats = start(nSols = 1, timeLimit = TIME_OUT)
    println(stats)

    finalTime = System.currentTimeMillis()

    val initTime = endInit - beforeInit
    val searchTime = finalTime - beforeSearch
    val totalTime = initTime + searchTime
    //println(stats)
    val time = stats.time
    if (time < TIME_OUT * 1000) {
      //println("   instance    implem    initTime  searchTime    totalTime   (stats.nFails-stats.nSols)  stats.nNodes  stats.nSols")
      println(s"${instance.split("\\/").last}\t$implem\t$initTime\t$searchTime\t$totalTime\t${stats.nFails-stats.nSols}\t${stats.nNodes}\t${stats.nSols}")
    }
    else {
      println(s"${instance.split("\\/").last}\t$implem\t$initTime\tT.0.\tT.0.\tT.0.\tT.0.\tT.0.")
    }
  }
  catch {
    case e: OutOfMemoryError => println(s"$instance\tMemory Out")
    case e: oscar.cp.core.NoSolutionException => {
      val initTime = endInit - beforeInit
      val searchTime = finalTime - beforeSearch
      val totalTime = initTime + searchTime
      println(s"${instance.substring(42)}\tNo solution")
    }
    case e: Exception => throw e
  }
}