package oscar.thesis.strhybrid

import oscar.algo.Inconsistency
import oscar.algo.reversible.ReversibleInt
import oscar.cp.core.delta.DeltaIntVar
import oscar.cp.core.variables.{CPIntVar, CPVar}
import oscar.cp.core.{CPPropagStrength, CPStore, Constraint}

/**
  * STR-Hybrid: restart and residues
  *
  */
class TableSTRHybrid(val variables: Array[CPIntVar], table: Array[Array[Int]]) extends Constraint(variables(0).store, "TableSTRHybrid") {

  override def associatedVars(): Iterable[CPVar] = variables

  idempotent = true
  priorityL2 = CPStore.MaxPriorityL2 - 1

  private[this] var timeStamp = 0
  private[this] val arity = variables.length
  private[this] val tableSize = table.length

  // Sparse set for current table (SSet)
  private[this] val dense = Array.tabulate(2 * tableSize)(i => i % tableSize)
  private[this] val sparse = Array.tabulate(tableSize)(i => i)
  private[this] val curLimitRev = new ReversibleInt(s, tableSize - 1)
  private[this] var prevLimit = -1

  // SSup, SVal
  private[this] var sSupLimit = -1
  private[this] var sValLimit = -1
  private[this] val sSup = Array.tabulate(arity)(i => i)
  private[this] val sVal = Array.tabulate(arity)(i => i)

  // Sparse set for values to remove (unsupported set)
  private[this] val unsupportedValues /* dense */ = Array.tabulate(arity)(i => new Array[Int](variables(i).size))
  private[this] val unsupportedPositions /* sparse */ = Array.tabulate(arity)(i => new Array[Int](variables(i).max - variables(i).min + 1))
  private[this] val unsupportedSizes /* size of each sparse set */ = new Array[Int](arity)
  private[this] val offsets /* used for position index */ = Array.tabulate(arity)(i => variables(i).min)

  // Last size of variables' domain since last invocation
  private[this] val lastSizes = Array.tabulate(variables.length)(i => new ReversibleInt(s, -1))

  // Data is using for new algorithm
  private[this] val data = new TableDataH(table)
  // Total table size for each variable (total sub-table of value in current variables' domain)
  private[this] val tableSizes = Array.tabulate(arity)(i => new ReversibleInt(s, tableSize))

  // DeltaDoms is difference between domain since last invocation
  private[this] val deltaDoms = new Array[DeltaIntVar](arity)
  private[this] val values = Array.ofDim[Int](variables.map(_.size).max) // store values during propagation process

  // Residues
  private[this] val isInSSup = new Array[Boolean](arity)
  private[this] val residues = Array.tabulate(arity)(i => Array.tabulate(variables(i).max - variables(i).min + 1)(j => -1))

  // Using for condition of propagation
  private[this] var minVarId = -1
  private[this] var minNbTupleAdd = Int.MaxValue
  private[this] var nbTupleDelete = 0

  override def setup(l: CPPropagStrength): Unit = {
    // Setup data
    data.setup()
    var i = 0
    while (i < arity) {
      variables(i).updateMax(data.max(i))
      variables(i).updateMin(data.min(i))
      i += 1
    }

    propagate() // first propagation

    i = 0
    while (i < arity) {
      deltaDoms(i) = variables(i).callPropagateOnChangesWithDelta(this)
      i += 1
    }
  }


  override def propagate(): Unit = {
    timeStamp += 1

    // Compute the minimum tableSize
    computeMinTableSize()

    prevLimit = curLimitRev.value
    val toReset = minNbTupleAdd < (prevLimit+1)
    val toEliminate = nbTupleDelete < (prevLimit+1) && timeStamp != 1

    setupSVal()
    var limit = -1

    // -- ELIMINATE --
    if (toEliminate) {
      // Sparse set must be setup for dense set
      setupSparseSet()

      // Remove invalid tuples with (x, a)
      limit = prevLimit
      var i = 0
      while (i <= sValLimit) {
        val varId = sVal(i)
        var k = deltaDoms(varId).fillArray(values)
        while (k > 0) {
          k -= 1
          limit = iterateXDiffA(varId, values(k), limit)
        }
        i += 1
      }

      // -- RESET --
    } else if (toReset) {
      // Sparse set must be setup for dense set
      setupSparseSet()

      // Reset sset and add valid tuples with (x, a)
      limit = -1
      var k = variables(minVarId).fillArray(values)
      while (k > 0) {
        k -= 1
        limit = iterateXEqA(minVarId, values(k), limit, prevLimit)
      }


      // -- STR2 --
    } else {
      limit = removeInvalidTuples()
    }

    if (toEliminate && prevLimit-limit < (limit+1)/3) {
      collectUnsupported(limit)
      findGACSupport(limit)
    } else {
      setupSSupAndUnsupported()
      collectGACValues(limit)
    }


    // Check for early terminate
    if (limit == -1) throw Inconsistency
    else curLimitRev.setValue(limit)

    if (sSupLimit == -1) return

    //------ Update domain ------
    updateDomain()
  }



  /**
    * Add valid tuples of subtable of (varId, value) to the current table (sset)
    *
    * @return new limit of SSet after adding valid tuples associate with (varId, value)
    */
  @inline private def iterateXEqA(varId: Int, value: Int, limit: Int, prevLimit: Int): Int = {
    var sSetLimit = limit
    var k = data.firstEq(varId, value)
    while (k != -1) {
      val tau = table(k)
      // Remark: can use SVal only with check in SSet
      if (isInSSet(k, prevLimit) && isValid(tau)) {
        sSetLimit += 1
        // -- Add tuple index k to SSet ---
        // pos1 = sSetLimit; val1 = dense(sSetLimit)
        // pos2 = sparse(k); val2 = k
        val val1 = dense(sSetLimit)
        val pos2 = sparse(k)
        swap(dense, sSetLimit, pos2)
        swap(sparse, val1, k)
      }
      k = nextEq(varId, k)
    }
    sSetLimit
  }

  /**
    * Remove tuples associate with (varId, value) from current table.
    *
    * @return new limit of SSet after removing tuples associate with (varId, value)
    */
  @inline private def iterateXDiffA(varId: Int, value: Int, limit: Int): Int = {
    var sSetLimit = limit
    var k = data.firstEq(varId, value)
    while (k != -1) {
      if (isInSSet(k, sSetLimit)) {
        // -- Remove tuple index k from SSet --
        // pos1 = sSetLimit; val1 = dense(sSetLimit)
        // pos2 = sparse(k); val2 = k
        val val1 = dense(sSetLimit)
        val pos2 = sparse(k)
        swap(dense, sSetLimit, pos2)
        swap(sparse, val1, k)
        sSetLimit -= 1

      }
      k = nextEq(varId, k)
    }
    sSetLimit
  }

  //-------------------------- Helper functions --------------------------

  @inline private def collectUnsupported(limit: Int): Unit = {
    // reset SSup, Unsupported
    sSupLimit = -1
    var varId = 0
    while (varId < arity) {
      isInSSup(varId) = false
      unsupportedSizes(varId) = 0
      varId += 1
    }

    var i = prevLimit
    while (i > limit) {
      val tid = dense(i) // tuple index
      val tau = table(tid)
      varId = 0
      while (varId < arity) {
        val variable = variables(varId)
        val value = tau(varId)
        val res = residues(varId)(value - offsets(varId))
        //println("DEBUG: " + value + " " + res + " " + limit)
        if (variable.hasValue(value) && (res == tid || !isInSSet(res, limit))) {
          // Residues
          isInSSup(varId) = true
          addToUnsupported(varId, value)
        }
        varId += 1
      }
      i -= 1
    }

    // add variable to SSup
    varId = 0
    while (varId < arity) {
      if (isInSSup(varId)) {
        sSupLimit += 1
        sSup(sSupLimit) = varId
      }
      varId += 1
    }

  }

  /**
    * Seek GAC support for each (variable, value) in Unsupported set
    */
  @inline private def findGACSupport(limit: Int): Unit = {
    var i = sSupLimit
    while (i >= 0) {
      val varId = sSup(i)
      val values = unsupportedValues(varId)
      var j = unsupportedSizes(varId) - 1
      while (j >= 0) {
        val value = values(j)
        val k = seekSupport(varId, value, limit)
        if (k != -1) {
          // Update residues
          residues(varId)(value - offsets(varId)) = k
          // remove value from Unsupported
          val newSize = removeFromUnsupported(varId, value)
          // remove varId from SSup
          if (newSize == 0) {
            sSup(i) = sSup(sSupLimit)
            sSupLimit -= 1
          }
        }
        j -= 1
      }
      i -= 1
    }
  }


  @inline private def seekSupport(varId: Int, value: Int, limit: Int): Int = {
    var k = data.firstEq(varId, value)
    while (k != -1) {
      //if (isValidTuple(table(k))) return k
      if (isInSSet(k, limit)) return k
      else k = nextEq(varId, k)
    }
    -1
  }

  @inline private def nextEq(varId: Int, tid: Int): Int = {
    data.nextEq(varId, tid)
  }

  /**
    * update Sparse Set for current table
    */
  @inline private def setupSparseSet() {
    var i = 0
    while (i <= prevLimit) {
      sparse(dense(i)) = i
      i += 1
    }
  }

  @inline private def isInSSet(tid: Int, limit: Int): Boolean = {
    if (tid < 0) return false // option
    sparse(tid) <= limit && dense(sparse(tid)) == tid
  }

  //----------------For STR2 --------------------------  
  /**
    * Iterate global table to remove invalid tuples of current tables
    *
    * @return new limit of global table
    */
  @inline private def removeInvalidTuples(): Int = {
    var i = 0
    var limit = curLimitRev.getValue()
    while (i <= limit) {
      val tau = table(dense(i))
      // Check tuple invalid to remove with SVal
      if (isValid(tau)) i += 1
      else {
        // removeTuple
        swap(dense, i, limit)
        limit -= 1
      }
    }

    limit
  }

  /**
    * Iterate global table to collect GAC values
    */
  @inline private def collectGACValues(limit: Int): Unit = {
    var i = 0
    while (i <= limit && sSupLimit >= 0) {
      val tid = dense(i)
      newSupport(table(tid), tid)
      i += 1
    }
  }

  /**
    * Collect GACValues of a valid tuple
    */
  @inline private def newSupport(tau: Array[Int], tid: Int): Unit = {
    var j = sSupLimit
    while (j >= 0) {
      val varId = sSup(j)
      // Update residues
      residues(varId)(tau(varId) - offsets(varId)) = tid

      val newSize = removeFromUnsupported(varId, tau(varId))
      // remove varId from SSup
      if (newSize == 0) {
        sSup(j) = sSup(sSupLimit)
        sSupLimit -= 1
      }
      j -= 1
    }
  }

  /**
    * Compute minTableSize for propagate
    */
  @inline private def computeMinTableSize(): Unit = {
    minNbTupleAdd = Int.MaxValue
    minVarId = -1
    nbTupleDelete = 0

    var varId = 0
    while (varId < arity) {
      // Update tableSize for variable whose domain is reduced
      val delta = deltaDoms(varId)
      if (delta != null && delta.changed) {
        // Test OK with lastSizes()
        var nDeletes = 0
        val delSize = delta.fillArray(values)
        var i = 0
        while (i < delSize) {
          nDeletes += data.subTableSize(varId, values(i))
          i += 1
        }
        nbTupleDelete += nDeletes
        tableSizes(varId) -= nDeletes
      }

      val tbSize = tableSizes(varId).getValue()
      if (minNbTupleAdd > tbSize) {
        minNbTupleAdd = tbSize
        minVarId = varId
      }
      varId += 1
    }

  }

  /**
    * Setup SVal
    */
  @inline private def setupSVal(): Unit = {
    // Reset SVal
    sValLimit = -1
    var varId = 0
    while (varId < arity) {
      // SVal
      if (variables(varId).size != lastSizes(varId).getValue) {
        // add variable's index to SVal
        sValLimit += 1
        sVal(sValLimit) = varId

        lastSizes(varId).setValue(variables(varId).size)
      }
      varId += 1
    }
  }

  @inline private def setupSSupAndUnsupported(): Unit = {
    // Reset SSup
    sSupLimit = -1
    var varId = 0
    while (varId < arity) {
      // Unsupported, SSup
      if (variables(varId).size > 1) {
        updateUnsupported(varId)
        // add variable's index to SSup
        sSupLimit += 1
        sSup(sSupLimit) = varId
      }
      varId += 1
    }
  }

  @inline private def updateDomain(): Unit = {
    var i = 0
    while (i <= sSupLimit) {
      val varId = sSup(i)
      val variable = variables(varId)
      val values = unsupportedValues(varId)
      val nValues = unsupportedSizes(varId)
      var reducedSize = 0

      var j = 0
      while (j < nValues) {
        variable.removeValue(values(j))
        reducedSize += data.subTableSize(varId, values(j))
        j += 1
      }

      tableSizes(varId).setValue(tableSizes(varId) - reducedSize)
      lastSizes(varId).setValue(variables(varId).size)
      i += 1
    }
  }

  @inline private def updateUnsupported(varId: Int): Unit = {
    // Copy the values
    val size = variables(varId).fillArray(unsupportedValues(varId))
    // Compute the positions
    val values = unsupportedValues(varId)
    val positions = unsupportedPositions(varId)
    unsupportedSizes(varId) = size
    var i = 0
    while (i < size) {
      positions(values(i) - offsets(varId)) = i
      i += 1
    }
  }

  /**
    * Remove (varId, value) from unsupported set.
    * @return size of unsupported set of varId
    */
  @inline private def removeFromUnsupported(varId: Int, val1: Int): Int = {
    val positions = unsupportedPositions(varId)
    val offset = offsets(varId)
    val pos1 = positions(val1 - offset)
    val size = unsupportedSizes(varId)
    if (pos1 >= size) size
    else {
      val values = unsupportedValues(varId)
      val pos2 = size - 1
      val val2 = values(pos2)
      values(pos1) = val2
      values(pos2) = val1
      positions(val1 - offset) = pos2
      positions(val2 - offset) = pos1
      unsupportedSizes(varId) = pos2
      pos2
    }
  }

  @inline private def addToUnsupported(varId: Int, value: Int): Unit = {
    val positions = unsupportedPositions(varId)
    val offset = offsets(varId)
    val pos = positions(value - offset)
    val size = unsupportedSizes(varId)
    if (pos >= size || unsupportedValues(varId)(pos) != value) {
      // add only if value is not is the set
      val values = unsupportedValues(varId)
      val newLimit = size
      values(newLimit) = value
      positions(value-offset) = newLimit
      unsupportedSizes(varId) = size + 1
    }
  }

  /**
    * Check tuple validity with SVal
    */
  @inline private def isValid(tau: Array[Int]): Boolean = {
    var i = 0
    while (i <= sValLimit) {
      val varId = sVal(i)
      if (!variables(varId).hasValue(tau(varId)))
        return false

      i += 1
    }
    true
  }

  @inline private def swap(arrays: Array[Int], i1: Int, i2: Int) = {
    val tmp = arrays(i1)
    arrays(i1) = arrays(i2)
    arrays(i2) = tmp
  }

}
